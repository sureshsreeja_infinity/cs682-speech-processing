function [ currentMinDistance, currentMinDistanceIndex ] = mindist( cVector, matrix )
%   Takes input a column vector and a matrix
%   (column in vector should be equal to rows of matrix)
%   Outputs the Minimum distance and index where to find it of the product between vector and each column of the matrix

    if (size(cVector, 2) > 1)
        error('Only column vectors allowed (m x 1)');
    end
    
    if (size(cVector, 1) ~= size(matrix,1))
        error('Collumn of vector should be equal to row of matrix');
    end
    
    
    currentMinDistance = Inf;
    currentMinDistanceIndex = 0;
    for i = 1:size(matrix,2)
        dist = euclid2vector(cVector,matrix(:,i));
        if dist<currentMinDistance
            currentMinDistance = dist;
            currentMinDistanceIndex = i;
        end
    end

end

