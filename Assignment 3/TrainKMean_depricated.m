function [ codebook ] = TrainKMeans( K, TrainingData, StoppingCriterion)
% Takes input K = number of codewords
%             TrainingData = training matrix of of size DxT (D =
%             dimensions, T = Observations)
%             StoppingCriterion = 
% Output = Codebook with K codewords

%-----------Randomly selecting initial codewords-----------
dimensions = size(TrainingData,1);
observations = size(TrainingData,2);
assumedCodebook = zeros(dimensions, K);
codebookLocations= randperm(observations, K);

for i = 1:K
    assumedCodebook(:,i) = TrainingData(:, codebookLocations(i));
end

done = 0;

distortionMatrix = zeros(1,observations);
for i = 1:observations
    distortionMatrix(i) = mindist(TrainingData(:,i),assumedCodebook);
end

old_distortion = mean(distortionMatrix)*2; %%added 0.1 to move past the first iteration
%display(assumedCodebook);
% codebook = assumedCodebook;
% while ~done
%     codebook(:,:) = 0;
%     [minDistMean, minDist, minCodeword] = KMeansQuantize(assumedCodebook, TrainingData);
%     for i = 1:K
%         
%         codebook(:,i) = mean(TrainingData(:,minCodeword==i)');
%     end
%     done = ((minDistMean/old_distortion)) >= StoppingCriterion;
%     old_distortion = minDistMean;
%     display(old_distortion);
%     assumedCodebook = codebook;
% end


dataInClusterCount = zeros(1, K);
codebook = assumedCodebook;
while ~done
    codebook(:,:) = 0;
    dataInClusterCount(:) = 0;
    distortionMatrix(:) = 0;
    for i = 1:observations
        [distortionMatrix(i), index] = mindist(TrainingData(:,i),assumedCodebook);
        codebook(:,index) = codebook(:,index) + TrainingData(:,i);
        dataInClusterCount(index) = dataInClusterCount(index) + 1;  
    end

    %updating codebook 
    for i = 1:K
        codebook(:,i) = codebook(:,i)/dataInClusterCount(i);
    end
    
    
    currentDistortion = mean(distortionMatrix);
    done = ((currentDistortion/old_distortion) >= StoppingCriterion);
    old_distortion = currentDistortion;
    display(old_distortion);
    assumedCodebook = codebook;
end
