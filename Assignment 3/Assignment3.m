%load test and train data
load('P3traintest.mat');

%generate codebooks for length 5 and 10
codebook5 = TrainKMeans(5, train{1,1}, 0.95);
codebook10 = TrainKMeans(10, train{1,1}, 0.95);

%Plot training data and respective codebooks
figure;
plot(train{1,1}(1,:),train{1,1}(2,:),...
    'LineStyle', 'none',...
    'Marker','.',...
    'MarkerSize',5);
hold on;
plot(codebook5(1,:),codebook5(2,:),...
    'LineStyle', 'none',...
    'Marker','d',...
    'MarkerSize',10,...
    'Color', 'r');
plot(codebook10(1,:),codebook10(2,:),...
    'LineStyle', 'none',...
    'Marker','*',...
    'MarkerSize',10,...
    'Color', 'g');
legend('Training Data','Codewords of a Codebook of Length 5','Codewords of a Codebook of length 10'); 
xlabel('MFCC c(i)');
ylabel('MFCC c(i)');
hold off;

%Plot test data and its corresponding clutering based on codebooks
figure;
[minMean, minDistance, codeword] = KMeansQuantize(codebook5, test{1,1});
colors = {'b', 'm', 'y', 'k','r'};
markers = {'+','o', '*', '.','x'};

plot(codebook5(1,:),codebook5(2,:),...
    'LineStyle', 'none',...
    'Marker','*',...
    'MarkerSize',20,...
    'Color', 'g');
hold on;
for i = 1:size(codebook5,2)
   plot(test{1,1}(1,codeword==i), test{1,1}(2,codeword==i),...
       'LineStyle', 'none',...
       'Marker', markers{i},...
       'Color', colors{i});
end

legend('Codewords','Clustered Test Data',...
    'Clustered Test Data','Clustered Test Data',...
    'Clustered Test Data','Clustered Test Data',...
    'Location', 'NorthEastOutside');
xlabel('MFCC c(i)');
ylabel('MFCC c(i)');
hold off;


%Generate average mean for different speakers and different Dimensional
%data

% codebook10_2D = TrainKMeans(10, train{1,1}, 1);
% codebook10_3D = TrainKMeans(10, train{1,2}, 1);
% codebook10_10D = TrainKMeans(10, train{1,3}, 1);
% [minMean2D_1, minDistance2D_1, codeword2D_1] = KMeansQuantize(codebook10_2D, test{1,1});
% [minMean2D_2, minDistance2D_2, codeword2D_2] = KMeansQuantize(codebook10_2D, test{1,2});
% [minMean3D_1, minDistance3D_1, codeword3D_1] = KMeansQuantize(codebook10_3D, test{2,1});
% [minMean3D_2, minDistance3D_2, codeword3D_2] = KMeansQuantize(codebook10_3D, test{2,2});
% [minMean10D_1, minDistance10D_1, codeword10D_1] = KMeansQuantize(codebook10_10D, test{3,1});
% [minMean10D_2, minDistance10D_2, codeword10D_2] = KMeansQuantize(codebook10_10D, test{3,2});

