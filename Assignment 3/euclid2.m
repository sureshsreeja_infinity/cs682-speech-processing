function [ distance ] = euclid2( vector1, vector2 )
%   Takes input two vectors of equal length
%   Returns squared euclidean distance between the two

    if (length(vector1) ~= length(vector2))
        error('Error! both vectors should have the same dimensions');
    end
    
    %diff = zeros(1, length(vector1)); 
    %for i = 1:length(vector1) %My implementation of vector subtraction
    %    diff = vector1(i) - vector2(i);
    %end
    
    diff = vector1 - vector2; %makes the code faster than my personal implementation
    distance = diff .^2;
    distance = sum(distance);

end

