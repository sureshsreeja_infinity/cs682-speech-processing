function Results = digitclassifier(N, method)
% Results = digitclassifier(N, method)
% Train and test using N codeword VQ models or N mixture GMMs
% Specify method as 'vq' or 'gmm'
% Returns Results structure:

% Verify arguments
narginchk(2,2); % # of inputs
switch method
    case {'vq', 'gmm', 'gmm-my'}
        % valid
    otherwise
        error('Unsupported classifier')
end

% UniqueSpeaker indicates whether only one exemplar
% of any given digit from any speaker is used.
UniqueSpeaker = false;

% Where is the feature data?
BaseDir = 'C:/Users/corpora/tidigits/mfc';

% Read in all filenames
[AllTrain, AllTest] = tidigits_isolated;

% Only retain files with this word in the path
% Set to [] to retain everything
RetainOnly = [];
%RetainOnly = '/woman';

if ~ isempty(RetainOnly)
    % We can use strfind to find things that match RetainOnly, but
    % the result is returned as a cell array with each entry indicating
    % the position where we found the substring. We would like everything
    % that is not empty. 
    % We can use cellfun to run a function on each cell and assemble the
    % results.  We negate the output to change ones to zeros and vice versa
    TrainWith = AllTrain(~ cellfun(@isempty, strfind(AllTrain, RetainOnly)));
    TestWith = AllTest(~ cellfun(@isempty, strfind(AllTest, RetainOnly)));
else
    TrainWith = AllTrain;
    TestWith = AllTest;
end

% Pull out the training files and organize them
% TrainFiles{n} are the files associated with class n
% TrainLabels{n} are the class labels

[TrainFiles, TrainLabels] = tidigits_splitfiles(TrainWith, UniqueSpeaker);

% When you are debugging, you may wish to prune these so that you
% are dealing with a smaller number of classes resulting in faster
% execution times.  Don't forget to remove it before you are done.
% e.g. Prune=4:length(TrainFiles);  % Keep the first 3 classes%
Prune = [];
%Prune=3:length(TrainFiles);

if ~ isempty(Prune)
    % Remove unwanted classes
    TrainFiles(Prune) = [];  
    TrainLabels(Prune) = [];
end

% Train models, Models{n} is the model for class n
Models = TrainRecognizer(TrainLabels, TrainFiles, method, N, BaseDir);

%Pull out and organize the test labels.
[TestFiles, TestLabels] = tidigits_splitfiles(TestWith, UniqueSpeaker);

if ~ isempty(Prune)
    % Remove unwanted test classes
    TestFiles(Prune) = [];
    TestLabels(Prune) = [];
end

% % Test against the models, results is a data structure containing
% % the error rate, confusion matrix, and any other needed data
% Results = Test(TestLabels, TestFiles, Models, method, BaseDir);
Results = Test(TestLabels, TestFiles, Models, method, BaseDir);




