function codebook = TrainKMeans(K, TrainingData, StopFraction)
% codebook = TrainKMeans(K, TrainingData, StopFraction)
%
% Given a matrix TrainingData where each column consists of one training
% sample, create a K mean codebook where each column of codebook is
% a codeword.
% 
% Training is done with the iterative Lloyd algorithm which is said to
% converge when the ratio of the current iteration's distortion to the 
% previous one is StopFraction x 100%.  StopFraction defaults to .99 when
% it is not specified.

if nargin < 3
    StopFraction = .95;  % set default
end

[Dim, N] = size(TrainingData); % N samples of dimension Dim


codebook = VQ_InitCodebook(TrainingData, K);

% Prime the loop --------------------

% Fudge a large improvement between the "previous iteration"
% (non-existant) and the current one so that we enter the loop and have
% "previous" values set correctly.
muDistortionPrevious = Inf;
DistortionRatio = 0;

% Find distortions for initial codebook
[muDistortion, MinDist, MinDistIdx] = KMeansQuantize(codebook, TrainingData);

% Iterate until good enough --------------------
iteration = 0;
while DistortionRatio < StopFraction
  
  % Generate new codebook
  for cwidx = 1:K
    
    % Find new centroid of each partition:
    % Take mean value across each row for vectors in partition cwidx.
    PartitionIndices = find(MinDistIdx == cwidx);
    if ~ isempty(PartitionIndices)
      codebook(:, cwidx) = mean(TrainingData(:,PartitionIndices), 2);
    end
  end

  % Find new distortions and closest codewords
  [muDistortion, MinDist, MinDistIdx] = ...
      KMeansQuantize(codebook, TrainingData);
  
  DistortionRatio = muDistortion / muDistortionPrevious; % New/old
  muDistortionPrevious = muDistortion;  % What is new becomes old
  
  iteration = iteration + 1;
%   fprintf('iteration %d Mean distortion %f, ratio to previous %f%%\n', ...
%           iteration, muDistortion, DistortionRatio*100);
end
