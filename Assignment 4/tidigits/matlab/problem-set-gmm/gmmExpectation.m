function gamma = gmmExpectation(gmm, data)
% Given a Gaussian mixture model and data matrix where each row is
% an observation, compute the expected contribution of each mixture k
% to the likelihood of each observation i and return it in gamma(k,i).

Mixtures = length(gmm.c);  % One prior for each mixture
T = size(data, 1);  % T row vectors of data
gamma = zeros(Mixtures, T);

% Begin by computing the likelihood of each observation with respect
% to a specific Gaussian, including the weight (prior) probability.
% You will need to use f_gauss and the weights here or gmmLikelihood
% with both outputs (model and mixture likelihoods)

% *** Complete me. ***

[P, PMixture] = gmmLikelihood(gmm,data); 

%divide each row element by rows sum to normalize
%using bsxfun makes it faster
gamma = bsxfun(@rdivide,PMixture,P);
gamma = gamma';


% Sanity check to help ensure your code is correct...
% (Make sure you understand why this works.  Note that
% we should be checking if it is equal to T, but there may
% be small rounding errors.)

assert(sum(sum(gamma)) - T < 1e-5, 'Expectation is not correct.');

    

