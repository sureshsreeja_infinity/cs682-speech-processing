function gmm = gmmCreate(Mixtures, TrainingData, DiagonalCovar)
% gmm = gmmCreate(Mixtures, TrainingData, DiagonalCovar)
% Create a Gaussian mixture model from training data.
%
% Inputs:
%       Mixtures - number of mixtures
%       TrainingData - N x D matrix where each of the N rows is a 
%               feature vector of dimension D
%       DiagonalCovar - If true, assumes that components of
%               training vectors are independent and off diagonal
%               covariance elements will be set to zero.
%               Note that it is very common to do this with
%               cepstral feature vectors as they can be shown
%               to be asymptotically independent with respect
%               to a wide class of signals (nearly anything that
%               you would be likely to observe).  In addition, 
%               matrix multiplication is greatly sped up by diagonal
%               matrices.
%           

% Make sure # of input arguments falls between the low and high value
error(nargchk(2,3,nargin));

if nargin < 3
  DiagonalCovar = true;
end


EMMaxIterations = 5;	% maximum number of iterations of EM algorithm
Verbose = 1;		% show progress = 1, quiet = 0


% Seed the model
gmm = gmmInit(Mixtures, TrainingData, DiagonalCovar);

% determine number vectors & their dimension
[TrainingCount, Dim] = size(TrainingData);		

EMCount = 1;	% first iteration
Stop = 0;	% termination condition

% Gamma_Mix_X_Train(k, i) Contribution of mixture k given 
% TrainingVector(i, :) to the overall probability of the
% training vector.  (Denoted by gamma_k^i in Huang et al.)
Gamma_Mix_X_Train = zeros(Mixtures, TrainingCount);

Pr = sum(log(gmmLikelihood(gmm, TrainingData)));

if Verbose
  fprintf('Seed log Pr = %f\n', Pr);
end

% Use EM algorithm to improve
while ~ Stop
  
  % Expectation ---------------
 
  % compute numerators of gamma_k(i):  P(y(i) | mixture k)
  % Note that in the matrix, the mixture is the row and the
  % i'th observation is in column i:  Gamma_Mix_X_Train(k,i)
  for mixture = 1:Mixtures
    Gamma_Mix_X_Train(mixture, :) = gmm.c(mixture) * ...
	f_gauss(TrainingData, gmm.means{mixture}, gmm.cov{mixture})';
  end
  
  % compute denominator: P(y(i) | model (all mixtures))
  Gamma_SumOfMixtures = sum(Gamma_Mix_X_Train);	

  % Divide each gamms_k(i) numerator by the denominator
  % Create copies of the matrix instead of looping.
  % This gives us the equation on slide 26 of the slides.
  Gamma_Mix_X_Train = ...
      Gamma_Mix_X_Train ./ repmat(Gamma_SumOfMixtures, Mixtures, 1);
  
  % We also need gmma_k which is the sum of gamma_k(i) across i.
  % We cna sum across the columns of Gamm_Mix_X_Train to compute this.
  Gamma_SumOfTrain = sum(Gamma_Mix_X_Train, 2);	% sum across columns
  
  % Maximization ---------------
  
  % Update weights
  gmm.c = Gamma_SumOfTrain ./ TrainingCount;
  for mixture = 1:Mixtures
    % Exploit the similarities between updating the mean and covariance
    % Create a matrix for the numerator to multiply each component
    % of the TrainingData or mean offset by a scalar.
    
    % Pull out mixture of interest and make it a column vector.
    Numerator = Gamma_Mix_X_Train(mixture, :)';
    % Replicate with Tony's trick, see
    % http://www.mathworks.com/support/tech-notes/1100/1109.html
    % We stamp out Dim copies of the Numerator
    Numerator = Numerator(:, ones(Dim, 1));

    % Update variance-covariance
    MeanOffset = ...
	TrainingData - repmat(gmm.means{mixture}, TrainingCount, 1);
    varcov = zeros(Dim);
    for i=1:TrainingCount
      varcov = varcov + Gamma_Mix_X_Train(mixture, i) * ...
	       MeanOffset(i,:)' * MeanOffset(i,:);
    end
    gmm.cov{mixture} = varcov / Gamma_SumOfTrain(mixture);

    % handle case where user wants to assume component wise independence
    if DiagonalCovar
      gmm.cov{mixture} = diag(diag(gmm.cov{mixture}));
    end
	
    % Update means
    gmm.means{mixture} = ...
        sum(Numerator .* TrainingData) ./ Gamma_SumOfTrain(mixture);
    
  end
  
  % determine new Pr
  OldPr = Pr;
  Pr = gmmLikelihood(gmm, TrainingData);
  Pr = sum(log(Pr));

  if Verbose	% display progress if so requested
    fprintf('Iteration %d ', EMCount);
    fprintf('log Pr = %f (%g improved from previous iteration) -----------\n', ...
	    Pr, Pr - OldPr);
    %gmmDisplay(gmm);
  end

  EMCount = EMCount + 1;        % another iteration done...

  % Check if we have converged or met the maximum
  % number of iterations
  if Pr <= OldPr | EMCount > EMMaxIterations
    Stop = 1;
  end
end


