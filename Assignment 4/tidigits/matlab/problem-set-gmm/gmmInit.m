function gmm = gmmInit(Mixtures, TrainingData, DiagonalCovar)
% gmm = gmmInit(Mixtures, TrainingData, DiagonalCovar)
% Initialize a Gaussian mixture model with the 
% specified number of Mixtures using row-vector
% training data provided in TrainingData.
% If DiagonalCovar is nonzero, the components of TrainingData(n,:)
% are assumed to be independent and off diagonals are set to 0. 

narginchk(3,3);  % Check number of arguments

% determine number vectors & their dimension
[TrainingCount, Dim] = size(TrainingData);		

% Build codebook with desired # of clusters and remember 
% which training vector is closest to each code word
[vq, ClosestCodeWords] = vqCreateModel(Mixtures, TrainingData);

% Set up parameters
gmm.c = zeros(Mixtures, 1);
gmm.means = cell(Mixtures, 1);
gmm.cov = cell(Mixtures, 1);

gmm.DiagonalCov = DiagonalCovar;      % full/diagonal covariance flag

for mixture = 1:Mixtures
  % Locate all training vectors associated with the mixture'th
  % codeword.
  Partition = find(ClosestCodeWords == mixture);
  
  % proportion of training vectors associated with this codeword
  gmm.c(mixture) = length(Partition) / TrainingCount;
  % sample mean, variance/covariance of the partition
  gmm.means{mixture} = mean(TrainingData(Partition, :));
  gmm.cov{mixture} = cov(TrainingData(Partition, :));
  if gmm.DiagonalCov;
    gmm.cov{mixture} = diag(diag(gmm.cov{mixture}));
  end
end





