function Results = Test(ClassLabels, TestSet, Models, Method, FeatureDir)
% Test() -  Perform speech recognition
%
%       TestData - Cell array with names of clases
%       TestSet - Cell array.  Each cell contains a cell array
%                 with the nams of test files for a specific class.
%       Models - Cell array of models to use for recognition
%       FeatureDir - All test file paths relative to this directory.

error(nargchk(5,5,nargin));     % need exactly N arguments

fprintf('Testing...\n');
ClassesN = length(Models);

% Data structures for tracking which utterances failed.

% Confusion(CorrectClass, ClassifiedAsClass)
% Store as a sparse matrix
Results.Confusion = sparse(ClassesN, ClassesN);

% Each row will contain a misclassified token.
% Column 1 indicates the correct class
% Column 2 indicates the token number within the class
% Column 3 indicates the class the decision function indicated.
%
% We initialize it as an empty matrix with 3 columns, this lets
% us add items with Results.Misclassified(end+1,:) = [new row vals];
Results.Misclassified = zeros(0, 3);

Results.TestCount = 0;
Results.MisclassificationCount = 0;

        
fprintf('Testing all tokens from class ');

scores = zeros(1, ClassesN);  % preallocate vector

%Append the extension to the test data
TestSet = cellfun(@(S) strcat(S,'_01.mfc'), TestSet, 'Uniform', 0);

tic  % time operation
for c = 1:ClassesN
  fprintf('%s ', ClassLabels{c});
  for t = 1:length(TestSet{c})
    % Read in features for this test...
    Features = [];
    Features = spReadFeatureDataHTK(TestSet{c,1}{1,t});

    % Determine score for each model
    for m = 1:ClassesN
      switch Method
        case 'vq'
            scores(m) = KMeansQuantize(Models(m).Codebook,Features);
        case 'gmm'
            scores(m) = hmmViterbi(Models(m).GMM,Features');         
        case 'gmm-my'
            scores(m) = sum(log(gmmLikelihood(Models(m).GMM,Features')));
        otherwise
            error('Unsupported modeling method %s', Method);
        end
    end
    
    % Use our decision rule to find the correct class
    switch Method
        case 'vq'
           [BestScore, BestClass] = min(scores); 
        case 'gmm'
           [BestScore, BestClass] = max(scores);
        case 'gmm-my'
           [BestScore, BestClass] = max(scores);
        otherwise
            error('Unsupported modeling method %s', Method);
    end
    

    % Update confusion matrix
    Results.Confusion(c, BestClass) =  Results.Confusion(c, BestClass) + 1;
    
    Results.TestCount = Results.TestCount + 1;
    Results.Misclassified(Results.TestCount,:) = [c t BestClass];    
  end



%   sizeOfEachEntry = zeros(1,length(TestSet{c,1}));
%   Features = [];
%   for t = 1:length(TestSet{c})
%     % Read in features for the entire label...
%     [d,i] = spReadFeatureDataHTK(TestSet{c,1}{1,t});
%     Features = [Features, d];
%     sizeOfEachEntry(t) = i.Vectors;
%   end
%     % Determine score for each model
%     for m = 1:ClassesN
%       switch Method
%         case 'vq'
%             scores(m) = KMeansQuantize(Models(m).Codebook,Features);
%         case 'gmm'
%             scores(m) = hmmViterbi(Models(m).GMM,Features');         
%         case 'gmm-my'
%             scores(m) = sum(log(gmmLikelihood(Models(m).GMM,Features')));
%         otherwise
%             error('Unsupported modeling method %s', Method);
%         end
%     end
%     
%     % Use our decision rule to find the correct class
%     switch Method
%         case 'vq'
%             for t = 1:length(TestSet{c})
%                 if t==1
%                 [BestScore(t), BestClass(t)] = min(scores(1:sizeOfEachEntry(t))); 
%                 else
%                 [BestScore(t), BestClass(t)] = min(scores(sizeOfEachEntry(t-1)+1:sizeOfEachEntry(t))); 
%                 end
%             end
%         case 'gmm'
%            [BestScore, BestClass] = max(scores);
%         case 'gmm-my'
%            [BestScore, BestClass] = max(scores);
%         otherwise
%             error('Unsupported modeling method %s', Method);
%     end
%     
% 
%     % Update confusion matrix
%     Results.Confusion(c, BestClass) =  sum(BestClass(BestClass==c));
%     
%     Results.TestCount = Results.TestCount + sum(Results.Confusion(c,:));
%    % Results.Misclassified(Results.TestCount,:) = [c t BestClass];    
%   


end
fprintf('\n');



Results.MisclassificationCount = Results.TestCount - sum(diag(Results.Confusion));
Results.ErrorRate = Results.MisclassificationCount/Results.TestCount
fprintf('error rate %f\n', Results.ErrorRate);

Results.ClassLabels = ClassLabels;

Elapsed_s = toc;	% note elapsed time 
fprintf('Elapsed test time %f m\n', Elapsed_s/60);


