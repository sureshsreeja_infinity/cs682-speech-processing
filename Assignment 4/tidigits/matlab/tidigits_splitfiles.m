function [SingleDigits, Classes] = tidigits_splitfiles(Files, UniqueSpeaker)
% [SingleDigits, Classes] = tidigits_splitfiles(Files, UniqueSpeaker)
% Given a cell array of files from the TIDigits corpus relative to the 
% root directory, sort the files into a pair of output arrays. 
% SingleDigits{i} contains all files related to the i'th class
% where i is one of the spoken digits plus "oh".
% Classes{i} contains the mapping from i to the class name as a text
% string.  (e.g. if Classes{i} is 'z", we know that class i represents
% the word zero.)
% 
% If the optional flag UniqueSpeaker is set to true, only the first
% instance of a class for any given speaker will be used.  As an example,
% if speaker kt produced two instances of the digit one, only one of them
% would be contained in the SingleDigits{1} if UniqueSpeaker == true.

% Check for correct # of input arguments
error(nargchk(1, 2, nargin));
if nargin < 2
    UniqueSpeaker = false;
end


Classes = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 'z', 'o'};
Nclasses = length(Classes);

% allocate cell arrays for each digit class train/test
SingleDigits = cell(Nclasses, 1); 

LastSpeaker = '';       % Dummy value for tracking last speaker.

% Split files into a cell array by class --------------------------
for idx = 1:length(Files)
    
    % We will assume that all filenames are well formed and
    % end with a single digit or o followed a letter a-z
    % indicating the instance.  

    % Find the index to the class character
    %classpos = regexp(Files{idx}, '[123456789oz][a-z]$');
    search = regexp(Files{idx}, ...
           '[/\\](?<name>[a-zA-Z])+[/\\](?<class>[123456789oz])[a-z]$', ...
           'names');
    if ~ isempty(search)
      
      if ~ strcmp(search.name, LastSpeaker)
        % new speaker, we haven't seen any exemplars for her/him.
        SpeakerClassCounts = zeros(Nclasses, 1);
        LastSpeaker = search.name;
      end

      switch search.class
       case 'o'
        classidx = 11;
       case 'z'
        classidx = 10;
       otherwise
        classidx = search.class - '0';
      end
      if ~ UniqueSpeaker || SpeakerClassCounts(classidx) == 0
        % Add to list for class unless we only want one
        % exemplar for each speaker.
        SingleDigits{classidx}{end+1} = Files{idx};
        % Note that we've seen it
        SpeakerClassCounts(classidx) = SpeakerClassCounts(classidx) + 1;
      end
      LastSpeaker = search.name;
    else
      error('Malformed file %s', Files{idx});
    end
  end
end
