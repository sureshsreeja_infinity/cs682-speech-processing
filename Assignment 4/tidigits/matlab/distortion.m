function distances = distortion(Data, CodeWords)
% distances = distortion(Data, CodeWords)
% Compute the pairwise Euclidean distortions between a 
% set of D column vectors (Data), and a set of C codewords
% (C column vectors of the same length).  
%
% distances(c,d) represents the Euclidean distortion
% between the c'th codeword and the d'th data vector.

% Although not necessary for correctness, we will preallocate the
% distortion matrix by creating a matrix of zeros.  This is faster than
% building the matrix incrementally.  The following line *can* be
% deleted, the program will just run slower.
distances = zeros(size(CodeWords, 2), size(Data, 2));

% We will compute distortion in an inefficient manner looping on the
% training data, it can be done much more efficiently, but is beyond the
% scope of what you have seen in Matlab.  (Hint, use repmat or even
% better Tony's trick.)

for didx = 1:size(Data, 2)
  for cwidx = 1:size(CodeWords, 2)
    % Difference between a training vector & code word
    difference = Data(:, didx) - CodeWords(:, cwidx);
    
    % Squared distance is the dot product of the difference with its
    % transpose.  We can use the built-in dot function or compute it as
    % follows: 
    squareddist = difference' * difference;
    
    % Build distortion matrix
    distances(cwidx, didx) = squareddist;
    
  end
end
