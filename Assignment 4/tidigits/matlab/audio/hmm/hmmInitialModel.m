function Model = hmmInitialModel(Model, states, mixtures, TrainingData, VarLimit)
% Model = hmmInitialModel(Model, states, mixtures, TrainingData, VarLimit)
% Constructs an initial Gaussian Mixture Model which should be refined
% by subsequent algorithms.  VarLimit specifies the value beneath which
% variances should not descend during the training process.
%
% State transition probabilities are random, and the gaussian means
% for each state are normally distributed about the mean for each
% state.
%
% This code is copyrighted 1997-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

Verbosity = 0;

[observations, components] = size(TrainingData);

% Normalized random state-transition matrix.
Model.a = rand(states);
Model.a = Model.a ./ repmat(sum(Model.a, 2), 1, states);

% Normalized initial state probability vector.
Model.pi = rand(states, 1);
Model.pi = Model.pi / sum(Model.pi);

% Normalized random mixture weights
Model.Mix.c = rand(mixtures, states);
Model.Mix.c = Model.Mix.c ./ repmat(sum(Model.Mix.c, 1), mixtures, 1);

% Use diagonal or full covariance matrices
Model.Mix.cov.Diagonal = 1;

if Model.Mix.cov.Diagonal
  Sigma = spdiags(diag(cov(TrainingData)), 0, components, components);
else
  % Use variance estimates from the training data.  
  % Use only the main diagonals for now.
  Sigma = diag(diag(cov(TrainingData)));
end
  
if Verbosity; fprintf('Generating state codebook\n'); end
% Create state level codebook.  If user has only requested one state, the
% distortion measure is irrelevant as the single codeword is simply the
% centroid. 

Codebook = vqCreateModel(states, TrainingData, ...
			'K-means', 'lbg', ...
			'Distortion', Model.Info.DistortionMetric, ...
			'Verbosity', Verbosity);

% For each state, assign one of the code-words.
% For the mixtures, permute the mean of each state
% specific code book by a normal deviate with mean
% 0 and a standard deviation to be within one standard deviation
% (modified by a scaling factor) of each component.

ScaleFactor = 3 * states;	% seat of the pants heuristic
Model.Mix.cov.Sigma = cell(mixtures, states);
stddev = sqrt(diag(Sigma));
Model.Mix.mu = zeros(Model.NumberMixtures, Model.Components, ...
		     Model.NumberStates);
for s=1:states
  for m=1:mixtures
    Model.Mix.mu(m,:,s) = Codebook.CodeWords(s,:)' + ...
	randn(components,1') .* stddev/ScaleFactor;
    Model.Mix.cov.Sigma{m,s} = Sigma;
  end
end

Model.Mix.limit = VarLimit;	% set variance limit
if (Model.Mix.limit > 0)

  % User has requested variance flooring aka clamping, limiting.
  % Compute global variances of training data and set thresholds
  % at Limit % of this.
  Model.Mix.LimitFloors = Model.Mix.limit * stVar(TrainingData)';
  
  % Make sure that variances do not exceed limits
  Model.Mix.cov.Sigma = hmmVarianceLimit(Model);
end

Model = hmmPreCompute(Model);
