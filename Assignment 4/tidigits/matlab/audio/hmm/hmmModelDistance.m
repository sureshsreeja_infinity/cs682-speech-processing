function Distance = hmmModelDistance(ModelA, ModelB, Length)
% Distance = hmmModelDistance(ModelA, ModelB, Length)
% Computes a symmetric similarity distance between two Hidden Markov
% Models.  Observation sequences from both models are generated and
% a distance measure based upon the probability of observing the sequences
% in each model is returned.  Note that repeated calls with the same
% parameters will return different results due to the random observation
% sequences.
% 
% The optional argument Length indicates the length of the observation
% strings to be generated.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,3,nargin));

if nargin < 3
  Length = 500;	% ad hoc default length
end

% Non symmetric distance measure:  normalized log prob ratio
% of model a to model b when observations generated from model b.
% D(MA, MB) = 1/T * [ log P(O-MB | MA) - log P(O-MB | MB)

% To make symmetric, we compute (D(MA, MB) + D(MB, M1)) / 2

ObsA = hmmGenObservations(ModelA, Length);	% observations from ModelA
LogProbAObsA = hmmViterbi(ModelA, ObsA);
LogProbBObsA = hmmViterbi(ModelB, ObsA);

ObsB = hmmGenObservations(ModelB, Length);	% observations from ModelB
LogProbAObsB = hmmViterbi(ModelA, ObsB);
LogProbBObsB = hmmViterbi(ModelB, ObsB);

Distance = ((LogProbAObsB - LogProbBObsB) + (LogProbBObsA - LogProbAObsA)) ...
    / (Length * 2);
