function [Alpha, AlphaScaling] = hmmalpha(Model, Data, Cache)
% Alpha = hmmAlpha(Model, Data)
% Computes a scaled alpha matrix for observation data.
% See Rabiner & Huang _Fundamental of Speech Recognition_ 1993
% pp 365-7 for information on scaled alpha values.
%
% Data should be a set of observerations where each column 
% is a different component and the rows range from time 1 to T.
%
% Cache is an optional argument containing a structure of precomputed
% probabilites.  
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,3,nargin));

if nargin < 3
  Cache = hmmProbCache(Model, Data, 'Mixtures', 0);
end

States = hmmModelStates(Model);
Time = size(Data, 1);	% Observations
Alpha = zeros(Time, States);
AlphaTrans = zeros(States, States);
AlphaScaling = ones(Time, 1);
Path = zeros(States);

% Indices used for replicating vectors across states/mixtures.
StateInd = ones(States,1);

t = 1;

% Compute a vector of observation probabilities for states 1..N
b = Cache.b(:,t);

% Intitialize Path vector.  Path(i) contains the probability
% of arriving in state i along any legal path through the 
% model.
Alpha(t, :) = (b .* Model.pi)' ;	% Initially pi_{i}

% normalize to prevent underflow - not really necessary with only
% one observation, but we'll do it just to be consistent
AlphaScaling(t) = 1 / sum(Alpha(t,:));
Alpha(t,:) = Alpha(t,:) * AlphaScaling(t);

lag1 = t;
t = t + 1;
while t <= Time

  % Observation probabilities for states 1..N
  b = Cache.b(:,t);
  
  % AlphaTrans_{j,i} = Probability of arriving in state i from
  %	state j taking into account probability of arriving in
  %	state j via all legal paths.
  AlphaTrans = Alpha(lag1*StateInd,:)' .* Model.a;
  
  % Path = Vector containing probability of arriving in a specific
  % state from any other state.    
  Path = sum(AlphaTrans);
  
  % Probability of being in state i at time t is the probability
  % of the model arriving at that along any valid path ending
  % in i * probability of seeing the observation in that state.
  Alpha(t,:) = Path .* b';
  
  % normalize to prevent underflow
  AlphaScaling(t) = 1 / sum(Alpha(t,:));
  Alpha(t,:) = Alpha(t,:) * AlphaScaling(t);
  
  lag1 = t;
  t = t+1;
end

debug=1;
