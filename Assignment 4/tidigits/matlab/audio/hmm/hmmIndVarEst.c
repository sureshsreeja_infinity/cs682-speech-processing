#include <mex.h>
#include <matrix.h>
#include <stdio.h>
#include <math.h>
#include "lib/hmmlib.h"

/* Optimization of variance reestimation for independent components 
 * 
 * This code is copyrighted 1998-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited.
 */

/* Unlike C, Matlab uses column major indexing */
#define COLMAJOR(ROW, COL, COLSIZE)	((ROW) + ((COL) * (COLSIZE)))

/* gateway function
 *
 * LHS Arguments:
 *	None
 * RHS Arguments:
 *	Model - Model.Mix.cov.Sigma will be overwritten
 *	gammamix(State,Mixture,Time)
 *	gammamix_LeaveState(State,Mix)
 *	Data(Time,Component)
 *
 * Maximizes probability of observation sequence through estimation
 * of variance.  Part of hmmBaumWelchGaussMix.
 *
 * This code is copyrighted 1998 by Marie Roch.
 * e-mail:  marie-roch@uiowa.edu
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */


void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray 
	    *prhs[])
{
  /* positional parameters */
  const int	Model = 0;
  const int	GammaMix = 1;
  const int	GammaMix_LeaveState = 2;
  const int	Obs = 3;
  
  double const	*MeanBase, *MeanPtr;
  double const	*ObsBase, *ObsPtr;

  int		States, Mixtures, Components, Time;
  int		m, s, c, t;

  mxArray	*MixInfo;
  mxArray	*CovInfo;
  mxArray	*MeanArray;
  mxArray	*SigmaCells;
  mxArray	*Sigma;
  double	*SigmaDiagPtr;
  double	*OffsetBase, *OffsetPtr, *VarianceBase, *VariancePtr;
  double	*GammaMixBase, *GammaMixPtr;
  double	*GammaMixLeaveStateBase, *GammaMixLeaveStatePtr;
    
  /* No error handling - should not be invoked directly by the user */

  /* Retrieve data */
  ObsBase = mxGetPr(prhs[Obs]);
  Time = mxGetM(prhs[Obs]);

  States = (int) mxGetScalar(mxGetField(prhs[Model], 0, "NumberStates"));
  Mixtures = (int) mxGetScalar(mxGetField(prhs[Model], 0, "NumberMixtures"));
  Components = (int) mxGetScalar(mxGetField(prhs[Model], 0, "Components"));
  Components = mxGetN(prhs[Obs]);

  MixInfo = mxGetField(prhs[Model], 0, "Mix");

  MeanArray = mxGetField(MixInfo, 0, "mu");
  MeanBase = mxGetPr(MeanArray);

  CovInfo = mxGetField(MixInfo, 0, "cov");
  SigmaCells = mxGetField(CovInfo, 0, "Sigma");
    
  GammaMixBase = mxGetPr(prhs[GammaMix]);
  GammaMixLeaveStateBase = mxGetPr(prhs[GammaMix_LeaveState]);
  
  /*
   * Check if model of same dimension as observation.  Assuming the
   * model is valid, this is the only check we should require.
   */
  if (mxGetN(prhs[Obs]) != Components) {
    mexErrMsgTxt("Feature data and model have different dimensionality.");
  }
  

  for (m=0; m<Mixtures; m++) {
    double	Variance_c;

    /* Access GammaMixLeaveState(1,m)
     * recall Matlab indices start at 1 and are stored in column major order
     */
    GammaMixLeaveStatePtr = GammaMixLeaveStateBase + COLMAJOR(0, m, States);

    for (s=0; s<States; s++) {

      ObsPtr = ObsBase;	/* Set to beginning of observations */
      MeanPtr = MeanBase;
      MeanPtr += m + s * Components * Mixtures;  /* mu(m,1,s) */

      /* Access Sigma(m,s) */
      Sigma = mxGetCell(SigmaCells, COLMAJOR(m, s, Mixtures));
      if (mxGetM(Sigma) != mxGetN(Sigma))
	mexErrMsgTxt("Variance matrix is not square");
      if (mxGetM(Sigma) != Components)
	mexErrMsgTxt("Size mismatch between components and variance matrix");
      SigmaDiagPtr = mxGetPr(Sigma);

      for (c=0; c < Components; c++) {
	Variance_c = 0;
	
	/* Access Gamma(s,m,1) */
	GammaMixPtr = GammaMixBase;
	GammaMixPtr += COLMAJOR(s, m, States);
      	
	for (t=0; t < Time; t++) {
	  double Offset_t = *ObsPtr++ - *MeanPtr;
	  Variance_c += *GammaMixPtr * Offset_t * Offset_t;
	  GammaMixPtr += Mixtures * States;
	} /* time */
	
	*SigmaDiagPtr++ = Variance_c / *GammaMixLeaveStatePtr;
	MeanPtr += Mixtures;		/* next component = next column */
      } /* component */
      GammaMixLeaveStatePtr++;	/* next state = next row */ 
    } /* state */
    
  } /* mixture */

  /* printf("There were %d output args\n", nlhs); */
}
