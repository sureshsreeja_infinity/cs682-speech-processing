function Result = hmmVerifyModel(Model)
% hmmVerifyModel(Model)
% Verifies stochastic constraints on a HMM model.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

Correct = 1;

% Initial probabilities must sum to 1.
if sum(Model.pi) ~= 1
  warning('hmmVerifyModel:  Pi distribution error\n');
  Correct = 0;
end

% Transition matrix destination state vectors must sum to 1.
for s=1:Model.NumberStates
  if sum(Model.a(s,:)) ~= 1
    warning(sprintf('hmmVerifyModel: row %d of A does not sum to 1\n'));
    Correct = 0;
  end
end

% Mixture weights for each state must sum to one.
for s=1:Model.NumberStates
  for m=1:Model.NumberMixtures
    if sum(Model.Mix.c(:,s)) ~= 1
      warning(sprintf('hmmVerifyModel:  Mixture wts %d do not sum to 1\n'));
      Correct = 0;
    end
  end
end

% Verify covariance matrices are positive definite
for s=1:Model.NumberStates
  for m=1:Model.NumberMixtures
    EigenVals = eig(Model.Mix.covSigma(:,:,m,s));
    if ~ isempty(find(EigenVals <= 0))
      warning(sprintf(['hmmVerifyModel:  Cov mat mix %d, state %d ', ...
	    'not positive definite'], m, s));
      Correct = 0;
    end
  end
end
