function Model = hmmCreateModel(States, Mixtures, TrainingData, Info, varargin)
% Model = hmmCreateModel(NumStates, NumMixtures, TrainingData, Info, OptArgs)
% Creates a HMM speaker model for Speaker with the specified number of
% states and mixtures based upon the row-oriented feature data in
% TrainingData.  The structure Info contains record structure user data
% which will be stored in Model.Info.  Additional information may be added
% to this record.
%
% Optional arguments
%       VarianceLimit, F in [0, 1]
%               Prevents variance from falling beneath F% of the total
%               variance.  Default 0.
%       Verbosity, N
%               0 - no output
%               1 - summary, warnings
%               2 - per iteration likelihoods
%
% If desired, the Info structure can contain information for performing
% user specified operations during model training every N iterations.  For
% details, see hmmBaumWelchIteration().
%
% This code is copyrighted 1997-2005 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

% For now, seed our random number generator with a known
% value so we can make sure to have repetition for debugging
% purposes.
% rand('state', 0);
% randn('state', 0);


% HOW TO RUN : hmmCreateModel(1, 5, train{1,1}', 'euclidean', 'Verbosity', 2)


error(nargchk(4,Inf,nargin));
if nargin < 5
  VarLimit = 0;
end

% defaults
Verbosity = 0;
VarLimit = 0;

k=1;
while  k <= length(varargin)
  switch varargin{k}
   case 'VarianceLimit'
    VarLimit = varargin{k+1};
    k=k+2;
   case 'Verbosity'
    Verbosity = varargin{k+1};
    k=k+2;
   otherwise
    error('Unknown option "%s"', varargin{k});
  end
end

[observations, components] = size(TrainingData);

Model.NumberStates = States;
Model.NumberMixtures = Mixtures;
Model.Components = components;

if ~ isfield (Info, 'DistortionMetric')
  Info.DistortionMetric = 'euclidean';
end

Model.Info = Info;
Model.Info.Iterations = 0;

% initial model with VQ codewords and random transition probabilities  
if Verbosity > 1, fprintf('preliminary VQ guess\n'); end
Model = hmmInitialModel(Model, States, Mixtures, TrainingData, VarLimit);

% refine mixtures by VQ clustering of vectors partitioned by state level
% Viterbi decoding .
if Verbosity > 1, fprintf('segmental K means\n'); end
Model = hmmSegmentalKMeans(Model, TrainingData);

% Perform Estimation Maximization training
if Verbosity > 1, fprintf('Baum Welch iteration.\n'); end
Model = hmmBaumWelchIteration(Model, TrainingData, 'Verbosity', Verbosity);

