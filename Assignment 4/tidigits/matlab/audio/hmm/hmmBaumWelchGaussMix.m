function Model = hmmBaumWelchGaussMix(Model, Alpha, Beta, Data, Cache)
% Model = hmmBaumWelchGuassMix(Model, Alpha, Beta, Data)
% Performs Baum-Welch (Expectation Maximization) model reestimation
% for continuous HMMs with distributions represented by Gaussian
% radial basis functions.
%
% Cache is an optional argument containing a structure of precomputed
% probabilities.  
%
% For details, see:
%
% Rabiner & Huang _Fundamentals of Speech Recognition_ 1993
%
% Paul, "Speech Recognition Using Hidden Markov Models", _The Lincoln
%	Laboaratory Journal, 1990, Vol 3, pp 41-62
%
% Baum, Petrie, Soules, Weiss, "A Maximation Technique Occuring in
%	the Statistical Analysis of Probabilistic Functions of
%	Markov Chains", _The Annals of Mathematical Statistics_, 
%	1970, Vol 14, No 1, pp 164-171.
%
% This code is copyrighted 1997-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(4,5,nargin));

if nargin < 5
  Cache = hmmProbCache(Model, Data);
end

iM = Model;	% For debug

% The variance reestimation method of Rabiner and Juang differs
% from that of Paul and Baum et al.  Rabiner/Juang base the variance
% estimate on a term by term difference between the current model mean and
% observation whereas Paul and Baum et al use the reestimated mean with the
% deviate from norm computed outside the observation summation. 
%
% A non zero value in RabinerJuangVar indicates that the RabinerJuang
% method should be used.
RabinerJuangVar = 1;

States = hmmModelStates(Model);
Mixtures = hmmModelMixtures(Model);
Debug = 0;

Time = size(Data, 1);	% Observations
Components = size(Data, 2);

% xi(i,j,t) is the probability of being in state i at time t and state
% j at time t+1.  Using Rabiner & Juang's notation
% where O_i represents the observation sequence, q_i the state sequence:
%
% Xi_t(i,j,m) = P(q_t = i, q_{t+1} = j, mixture_m, O | Model) / P(O | Model)
% We also need to know the probabilities for each mixture, but we do
% not need to retain the values over time.

xi = repmat(Model.a, [1, 1, Time-1]);
ximix = zeros(States, States, Mixtures);

% Indices used for replicating vectors across states/mixtures.
StateInd = ones(States,1);
MixInd = ones(Mixtures,1);

if Time - 1 < 1
  error('Insufficient training data\n');
end

if Debug
  fprintf(1, 'Baum-Welch reestimation\n');
end

% Expectations -----------------------------------------------------------

% xi - probability of transitioning through a pair of states i, j
for t=1:(Time-1)
  b = Cache.b(:,t+1);
  bmix = Cache.bmix(:,:,t+1);

  % Compute probability w/o taking into account observation
  AlphaAijBeta = Alpha(t*StateInd,:)' .* Model.a .* Beta((t+1)*StateInd,:);

  % Add in probability for all mixtures
  xi(:,:,t) = AlphaAijBeta .* b(:,StateInd)';

  % P(O | Model) - The observation probability can be obtained by
  % summing the rows and columns of the xi matrix.  When the alpha and
  % beta values are scaled, this number is a scaled probability.  When
  % scaling is enabled at each time step, this value is 1.
  ObsProb = sum(sum(xi(:,:,t)));
  % Don't waste operations dividing by 1:
  if ObsProb ~= 1
    % Scale the xi matrices by the (scaled) observation probability.
    xi(:,:,t) = xi(:,:,t) / ObsProb;
  end
end

% Compute gamma:  Probability of leaving a state at time t.
%
% gamma_t(i) - Probability of leaving state i at time t [Eqn 6.38 p 343]
%	= sum_{j=1}^{States} xi_t(i,j) [Eqn 6.38]
%	= sum_{j=1}^{States} 
%		  alpha_t(i) a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)
%		  -----------------------------------------------
%		  sum_{i=1}^{States} sum{j=1}^{States} 
%		  alpha_t(i) a_{i,j} b_j{o_{t+1}) beta_{t+1}(j))
%
% Recall that:
%
%	alpha_1(i) = pi_i and for t = 1,...,T-1
%	alpha_{t+1}(i) = (sum_{i=1}^{States} alpha_t(i) a_{i,j}) b_j(o_{t+1})
%
%	changing variables t for t+1:  for t=2..T
%	alpha_{t}(i) = (sum_{i=1}^{States} alpha_{t-1}(i) a_{i,j}) b_j(o_t)
% and
%	beta_T(i) = 1 and for t=T-1,...,1
%	beta_t(i) = sum_{j=1}^{States} a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)
%
% The denominator of gamma_t(i) does not change with respect to the
% outmost summation and can be factored out:
%	
% gamma_t(i)
%	=    [      1
%		---------------------------------------------------	
%		sum_{i=1}^{States} sum{j=1}^{States} 
%		alpha_t(i) a_{i,j} b_j{o_{t+1}) beta_{t+1}(j))		]
%	
%	     * sum_{j=1}^{States} alpha_t(i) a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)
%
%	Let us consider the summation:
%	sum_{j=1}^{States} alpha_t(i) a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)
%	= alpha_t(i) [sum_{j=1}^{States} a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)]
%	= alpha_t(i) beta_t(i)
%
% gamma_t(i) =
%		alpha_t(i) beta_t(i)
%		---------------------------------------------------	
%		sum_{i=1}^{States} alpha_t(i) beta_t(j)
%
% When the observation distribution is modeled by a mixture model, we can
% also discuss the probability of leaving a state through a specific
% mixture.  (Not really what happens, but a useful concept for
% reestimation.)
%
% gamma_t(i,m) - Probability of leaving state i through mixture m @ time t.
%		[Unlabeled Eqn, p352, changed mixture index k to m]
%	= [alpha_t(j)*beta_t(j) / sum_{j=1}^{States} alpha_t(j)*beta_t(j)]
%	 * [c_{j,m} b_{t,m}(O_t) / sum_{m=1}^{Mixtures} c_{j,m} * b_{t,m}(O_t) ]
%

gamma = zeros(States, Time);
gammamix = zeros(States, Mixtures, Time);

% Compute probability of being in a state accounting for t'th observation
AlphaBeta = Alpha .* Beta;
AlphaBetaSums = sum(AlphaBeta, 2);	% sum probs for each time t

for t=1:Time
  b = Cache.b(:,t);
  bmix = Cache.bmix(:,:,t);

  if Debug
    fprintf('P(b(%d))\n', t); b, bmix
  end

  gamma(:,t) = AlphaBeta(t,:)' / AlphaBetaSums(t);
  gammamix(:,:,t) = ...
      gamma(:,t(ones(Mixtures,1))) .* (bmix' ./ b(:,MixInd));
end

if Debug
  xi
  gamma
  gammamix
end

% Maximization -----------------------------------------------------------

% Pi is expected frequency in state i at time t=1 (gamma_1{i})
Model.pi = gamma(:,1);

% a_{i,j} = expected frequency of transitions i to j /
%		expected frequency leaving state i
% NOTE:  Formula modified slightly from Rabiner & Juang.  Derived directly
% from xi matrix rather than using xi and gamma.  Small rounding errors in
% the xi/gamma matrices prevent proper scaling of probabilities to unity.
Freqij = sum(xi,3);
Leavei = sum(Freqij, 2);
Model.a = Freqij ./ Leavei(:, StateInd);

gammamixSumOverTime = sum(gammamix, 3);

% Mixture weight reestimation
% New weight for each state j and mixture k is determined
% by the relative frequency of occurrences with the current
% parameters.  For each state, we examine how many times
% (probabilistic count) we left the state through mixture k
% during the observation sequence and divide by the number of
% times that we left that state through all mixtures.

% c(mix, state)
LeaveStateS = sum(gamma, 2)';	% Sum over time
Model.Mix.c = gammamixSumOverTime' ./ LeaveStateS(MixInd, :);

% For now kludge in different types of reestimation, if
% it works, we'll do it better
if RabinerJuangVar

% Mean & Variance reestimation.
% For both of these, we will be looking at relative probabilistic sums
% which will need to be normalized.  In both cases, we'll normalize
% by the using the sum probabilistic number of times that we leave
% state i through a mixture m.  This is obtained by summing the gammamix
% matrix over time.
gammamix_LeaveState_X_Mix = squeeze(gammamixSumOverTime);

% Variance reestimation

if Model.Mix.cov.Diagonal
  
  % IndVarEst
  % hmmIndVarEst estimates variance and overwrites Sigma, a violation
  % of Matlab function protocol (avoids overhead of copying)
  hmmIndVarEst(Model, gammamix, gammamix_LeaveState_X_Mix, Data);
  
else
  
  % No independence assumption.
  ZeroComponents = zeros(Components, Components);
  for s=1:States
    for m=1:Mixtures
      Covariance = ZeroComponents;
      for t=1:Time
	Offset = Data(t,:) - Model.Mix.mu(m,:,s);
	Covariance = Covariance + gammamix(s,m,t) * Offset' * Offset;
      end
      Model.Mix.cov.Sigma{m,s} = Covariance / gammamix_LeaveState_X_Mix(s,m);
    end
  end
  
end

if (Model.Mix.limit > 0)
  % Make sure that variances do not exceed limits
  Model.Mix.cov.Sigma = hmmVarianceLimit(Model);
end
Model = hmmPreCompute(Model);


% Mean Reestimation - mu[mixture, component, state]
% We reestimate the means by taking the normalized sum of observations
% weighted by the probability of leaving a specific state, mixture,
% and time (gammamix).

% Preallocate a state specific mean structure:  Components X Mixtures
% We will have to transpose this to store it in the model which
% expects Mixtures X Components, but setting it up this way is more
% efficient as we calculate the mixtures for each component.
statejmu = zeros(Components, Mixtures);
MixIndices=ones(1, Mixtures);	% Used for replication
CompIndices=ones(1, Components);
for j=1:States
  % We need to access the cross section of the gammamix (State X Mix X
  % Time) matrix which pertains to the current state.  For easier
  % access, the cross section will be stored as Time X Mixture.
  % jgammamix = squeeze(gammamix(j,:,:))';
  jgammamix = reshape(permute(gammamix(j,:,:), [3 2 1]), [Time, Mixtures]);
  LeaveThroughState_j = gammamix_LeaveState_X_Mix(j,:);

  for k=1:Components
    ComponentK = Data(:,k);
    statejmu(k,:) = sum(ComponentK(:,MixIndices) .* jgammamix);
  end
  
  statejmu = statejmu ./ LeaveThroughState_j(CompIndices,:);
  Model.Mix.mu(:,:,j) = statejmu';
end

else
  % Reestimate mean and variance according to Baum et al.
  % Estimate mean first and use new estimate for variance update
  
  % Mean & Variance reestimation.
  % For both of these, we will be looking at relative probabilistic sums
  % which will need to be normalized.  In both cases, we'll normalize
  % by the using the sum probabilistic number of times that we leave
  % state i through a mixture m.  This is obtained by summing the gammamix
  % matrix over time.
  gammamix_LeaveState_X_Mix = squeeze(gammamixSumOverTime);
  
  % Mean Reestimation - mu[mixture, component, state]
  % We reestimate the means by taking the normalized sum of observations
  % weighted by the probability of leaving a specific state, mixture,
  % and time (gammamix).
  
  % Preallocate a state specific mean structure:  Components X Mixtures
  % We will have to transpose this to store it in the model which
  % expects Mixtures X Components, but setting it up this way is more
  % efficient as we calculate the mixtures for each component.
  statejmu = zeros(Components, Mixtures);
  MixIndices=ones(1, Mixtures);	% Used for replication
  CompIndices=ones(1, Components);
  for j=1:States
    % We need to access the cross section of the gammamix (State X Mix X
    % Time) matrix which pertains to the current state.  For easier
    % access, the cross section will be stored as Time X Mixture.
    jgammamix = squeeze(gammamix(j,:,:))';
    LeaveThroughState_j = gammamix_LeaveState_X_Mix(j,:);
    
    for k=1:Components
      ComponentK = Data(:,k);
      statejmu(k,:) = sum(ComponentK(:,MixIndices) .* jgammamix);
    end
    
    statejmu = statejmu ./ LeaveThroughState_j(CompIndices,:);
    Model.Mix.mu(:,:,j) = statejmu';
  end
  
  % Variance reestimation
  if Model.Mix.cov.Diagonal
    
    % Assume independence, optimize computation
    ZeroComponents = zeros(1, Components);
    for s=1:States
      for m=1:Mixtures
	Covariance = ZeroComponents;
	for t=1:Time
	  Covariance = Covariance + gammamix(s,m,t) * (Data(t,:) .* Data(t,:));
	end
	Covariance = Covariance / gammamix_LeaveState_X_Mix(s,m) ...
	    - Model.Mix.mu(m,:,s) .* Model.Mix.mu(m,:,s);
	% store main diagonal as sparse matrix
	Model.Mix.cov.Sigma{m,s} = ...
	    spdiags(Covariance', 0, Components, Components);
      end
    end
    
  else
    
    % No independence assumption.
    ZeroComponents = zeros(Components, Components);
    for s=1:States
      for m=1:Mixtures
	Covariance = ZeroComponents;
	for t=1:Time
	  Covariance = Covariance + gammamix(s,m,t) * Data(t,:)' * Data(t,:);
	end
	Model.Mix.cov.Sigma{m,s} = ...
	    Covariance / gammamix_LeaveState_X_Mix(s,m) ...
	    - Model.Mix.mu(m,:,s)' * Model.Mix.mu(m,:,s);
      end
    end
  
end
  
end
