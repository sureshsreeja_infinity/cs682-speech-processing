/*
 * hmmEMStep.c
 *
 * Compute one iteration of the Expectaton Maximization algorithm.
 * We will assume independendence between components.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#include <mex.h>
#include <matrix.h>
#include <math.h>
#include "lib/hmmlib.h"
#include "lib/hmmModelAccessor.h"

/*
 * Matlab gateway function hmmEMStep()
 * Compute one step of the expectation maximization (EM) algorithm
 * as applied to hidden Markov models (HMMs).
 *
 * See hmmEMStep.m for documentation
 */

void
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  /* parameter positions  ----------------------------------------*/

  /* in */
  const int	ObsPos = 0;
  const int	AlphaPos = 1;
  const int	BetaPos = 2;
  const int	InModelPos = 3;
  const int	InputArgCount = 4;

  /* out */
  const int	OutModelPos = 0;
  const int	OutputArgCount = 1;

  double 	*Obs, *Obs_t;
  HMM		Model;		/* current model */
  HMM		ModelP;		/* new model' */
  int		T;		/* number of observations */
  int		t;		/* time index */
  int		s, sFrom, sTo;	/* state indices */
  int		m;		/* mixture index */
  int		c;		/* component index */

  double	*Xi;		/* Sum over time of all Xi_t's */
  double	PrO_t;		/* Probability of seeing O_t at time t */
  double	OffsetFromMean;

  double	*Gamma;
  double	*Gamma_t;
  double	*GammaMix;
  double	*GammaMix_t;
  double	*PrMixture_ti;	/* Pr(O_t | q_t=i)[mixture] */
  double	*Alpha;
  double	*Beta;
  
  /* Minimal error handling - should not be invoked directly by the user */

  if (nrhs != InputArgCount)
    mexErrMsgTxt("Bad number of input arguments.");
  if (nlhs != OutputArgCount)
    mexErrMsgTxt("Bad number of output arguments.");
      
  
  
  /* Retrieve data */
  Obs = mxGetPr(prhs[ObsPos]);
  T = mxGetN(prhs[ObsPos]);	/* Number of observations */

  /* Retrieve input model */
  hmmModelAccessor(prhs[InModelPos], &Model, ALLOC_MATLAB);

  /* Retrieve Alpha & Beta */
  Alpha = mxGetPr(prhs[AlphaPos]);
  Beta = mxGetPr(prhs[BetaPos]);
  
  /*
   * Check if:
   *	model and observation have same number of components.
   *	in and out models have the same number of states & mixtures
   *	Alpha & Beta have the right number 
   *
   * These are only elementary checks to make sure the caller did not
   * invoke the routine with grossly incorrect parameters.  This will
   * not catch all problems.  Since the routine is not intended to be
   * called from anything other than an EM driver, this should not
   * create large problems.
   */

  if (mxGetM(prhs[ObsPos]) != Model.Components) {
    hmmFreeModelAccessor(&Model, ALLOC_MATLAB);
    hmmFreeModelAccessor(&ModelP, ALLOC_MATLAB);
    mexErrMsgTxt("Size mismatch between observation and Model dimensions");
  } 

  /* Everything looks allright, create output model and initialize */
  plhs[OutModelPos] = mxDuplicateArray(prhs[InModelPos]);
  hmmModelAccessor(plhs[OutModelPos], &ModelP, ALLOC_MATLAB);

  /* ----------------------------------------------------------------------
   * Xi = probability transition from state i to j
   *	= sum_t Xi_t(i,j) / [sum_t sum_j Xi_t(i,j) ]
   * Xi_t(i,j) = likelihood of transitioning from state i to j at time t.
   *
   * The new state transition matrix A is set to Xi.
   */

  /* Allocate & compute Xi - expected transition counts form state i to j */
  if (! (Xi = mxCalloc(Model.States * Model.States, sizeof(double))))
    mexErrMsgTxt("Unable to allocate Xi");
  
  /* Xi(i,j) = sum over time Xi_t(i, j)
   * Xi_t(i,j) (not retained) = P(q_t = i, q_{t+1}=j, O | Model)
   *				--------------------------------
   *					P(O | Model)
   */
  hmmXi(Xi, Alpha, Beta, &Model, Obs, T);


  /* Maximize the transition matrix a:
   *	a(i,j) = sum_{t=1}^{t=T} expected number of transitions
   *				 from state i to state j.
   *		 ----------------------------------------------
   *		  expected number of transitions from state i
   *
   *	       = sum_{t=1}^{t=T} Xi_t(i,j)    Xi(i,j)
   *		 ------------------------- = -----------
   *		 sum_{t=1}^{t=T} Gamma_t(i)   Gamma(i)
   *
   *		= Xi(i,j)
   *		  ----------------------
   *		  sum_{s=1}^N Xi(i, s)
   *
   */
  for (sFrom = 0; sFrom < Model.States; sFrom++) {
    double SumTo = 0.0;
    
    /* compute denominator for this from state */
    for (sTo = 0; sTo < Model.States; sTo++)		/* sum */
      SumTo += *ROW_ACCESS(Xi, sFrom, sTo, Model.States);

    /* Normalize each row and use the resulitng probability as
     * an estimate for the new transition matrix
     */
    for (sTo = 0; sTo < Model.States; sTo++)
      *COL_ACCESS(ModelP.APtr, sFrom, sTo, Model.States) =
	*ROW_ACCESS(Xi, sFrom, sTo, Model.States) / SumTo;
  }


  /* ----------------------------------------------------------------------
   * Gamma
   * GammaMix
   */

  Gamma_t = mxMalloc(Model.States * sizeof(double));
  Gamma = mxMalloc(Model.States * sizeof(double));
  GammaMix_t = mxMalloc(Model.States * Model.Mixtures * sizeof(double));
  GammaMix = mxMalloc(Model.States * Model.Mixtures * sizeof(double));

  /* Probability of mixtures for a given time t, state i */
  PrMixture_ti = mxMalloc(Model.Mixtures * sizeof(double));

  /* Zero:
   *  mean & covariance locations as we will use these 
   *  locations to cumulatively build the new covariance.
   *
   *  GammaMix - Will hold the sum of the GammaMix_t's
   *  Gamma - Will hold sum of Gamma_t's
   */
  for (s = 0; s < Model.States; s++) {
    Gamma[s] = 0.0;
    for (m = 0; m < Model.Mixtures; m++) {
      *ROW_ACCESS(GammaMix, s, m, Model.Mixtures) = 0.0;
      for (c = 0; c < Model.Components; c++) {
	/* means */
	*COL_ACCESS3(ModelP.MeansPtr, m, c, s, Model.Mixtures, Model.Components) = 0.0;
	
	/* variance */
	(*COL_ACCESS(ModelP.Sigma, m, s, Model.Mixtures))[c] = 0.0;
      }
    }
  }
  
  Obs_t = Obs;	/* point to first observation */
  for (t = 0; t < T; t++) {
    double	AlphaBetaSum = 0.0;

    /* Sum likelihoods of being in each state i for a specific time t */
    for (s = 0; s < Model.States; s++)
      AlphaBetaSum += *COL_ACCESS(Alpha, t, s, T) *
	*COL_ACCESS(Beta, t, s, T);

    /* Gamma_t(i) - probability of leaving state i at time t.
     * GammaMix_t(i, m) - probability of leaving state i through
     *		mixture m at time t.
     */
    for (s = 0; s < Model.States; s++) {
      double	MixtureSum = 0.0;
      
      /* Probability of leaving state i at time t:
       *	Prob entering i * Prob leaving i at time t 
       *	-------------------------------------------------
       *	sum_i (Prob entering i * Prob leaving i at time t)
       */
      Gamma_t[s] =
	*COL_ACCESS(Alpha, t, s, T) * *COL_ACCESS(Beta, t, s, T) /
	AlphaBetaSum;

      Gamma[s] += Gamma_t[s];	/* sum over time */

      /* Probability for each mixture
       * Scale Gamma_t[s] by contribution of each mixture to the
       * overall likelihood
       */
      hmm_f_state(s, &Model, PrMixture_ti, Obs_t);

      /* Sum likelihoods of mixtures, note that this is returned by
       * hmm_f_state, but it will set 0 probability mixtures (not
       * possible except in case of underflow) to the minimum value in
       * IEEE arithmetic.  We'd like to include this in the unlikely
       * event that it happened, so we compute the sum manually.
       */
      for (m = 0; m < Model.Mixtures; m++)
	MixtureSum += PrMixture_ti[m];

      /* Probability of leaving state i through mixture m at
       * time t GammaMix_t[i][m]:
       *
       *	Prob of leaving state i at time t * contribution mixture m
       *	----------------------------------------------------------
       *			Sum of mixture contributions
       *
       * Probability of leaving state i at time t
       *	Gamma_t[i] = sum over mixtures GammaMix_t[s][m]
       */
      for (m = 0; m < Model.Mixtures; m++) {
	
	*ROW_ACCESS(GammaMix_t, s, m, Model.Mixtures) =
	  Gamma_t[s] * PrMixture_ti[m] / MixtureSum;

	/* Accumulate GammaMix_t into GammaMix */
	*ROW_ACCESS(GammaMix, s, m, Model.Mixtures) +=
	  *ROW_ACCESS(GammaMix_t, s, m, Model.Mixtures);
	  
	/*
	 * Mean(s,m) reestimation =
	 *
	 *  sum_t (GammaMix_t(s,m) * obs_t)
	 *  --------------------------------
	 *	sum_t GammaMix_t(m,s)
	 *
	 * Covariance(s,m) reestimation =
	 *  sum_t ((obs_t - mu(s,m)) * GammaMix_t(s,m) * (obs_t - mu(s,m))')
	 *  ----------------------------------------------------------------
	 *		sum_t GammaMix_t(m,s)
	 *
	 * Since we assume that the components are independent, this
	 * matrix multiplication can be simplified to a product of scalars.
	 *
	 * Here we will accumulate the numerators for state s,
	 * mixture m, time t.
	 */
	for (c=0; c < Model.Components; c++) {
	  double	OffestFromMean;

	  /* Mean */
	  *COL_ACCESS3(ModelP.MeansPtr, m, c, s,
		       Model.Mixtures, Model.Components) +=
	    *ROW_ACCESS(GammaMix_t, s, m, Model.Mixtures) * Obs_t[c];

	  /* Variance */
	  OffsetFromMean = Obs_t[c] -
	    *COL_ACCESS3(Model.MeansPtr, m, c, s,
			 Model.Mixtures, Model.Components);
	  (*COL_ACCESS(ModelP.Sigma, m, s, ModelP.Mixtures))[c] +=
	    (OffsetFromMean * OffsetFromMean) *
	    *ROW_ACCESS(GammaMix_t, s, m, Model.Mixtures);
	}
      }
    } /* end for state */

    /* Initial Pr is the likelihood of leaving state s at time 0 */
    if (t == 0) {
      for (s = 0; s < Model.States; s++)
	*COL_ACCESS(ModelP.APtr, s, 0, ModelP.States) =
	  Gamma_t[s];
    }

    /* Set pointers for next observation */
    COL_INCCOL(Obs_t, Model.Components, T);
  } /* end for time */

  /*
   * Reestimation of mean and variance.  Numerator has been taken care
   * of above, divide by denominator now that it is available.
   *
   * Reestimation of mixture weight:
   *	c_{m,s} = GammaMix_{m, s} / (sum_m GammaMix_{s,m})
   */
  for (s = 0; s < Model.States; s++) {
    for (m = 0; m < Model.Mixtures; m++) {

      /* mixture weight */
      *COL_ACCESS(ModelP.MixtureWtsPtr, m, s, ModelP.Mixtures) =
	*ROW_ACCESS(GammaMix, s, m, Model.Mixtures) /
	Gamma[s];
      
      for (c = 0; c < Model.Components; c++) {
	/* mean */
	*COL_ACCESS3(ModelP.MeansPtr, m, c, s,
		     ModelP.Mixtures, ModelP.Components) /=
	  *ROW_ACCESS(GammaMix, s, m, Model.Mixtures);

	/* variance */
	(*COL_ACCESS(ModelP.Sigma, m, s, ModelP.Mixtures))[c] /=
	  *ROW_ACCESS(GammaMix, s, m, Model.Mixtures);
      }
    }
  }
}
