Documentation is in the LaTeX design.tex

Check the \usepackage directive to see which packages I use.  You may
need to obtain them from a CTAN site if your cite does not have these
installed.
