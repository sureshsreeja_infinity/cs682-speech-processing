/*
 * hmmPersistentDestory.c
 *
 * Remove all previously cached models.
 *
 * This code is copyrighted 2003 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */


#include <mex.h>
#include <matrix.h>
#include <math.h>

#include <dlfcn.h>

#include <sys/types.h>
#include <unistd.h>

#include "lib/hmmlib.h"
#include "lib/soException.h"

#include <utSharedLib.h>
#include <MxUtil.h>

/* Name of field that will indicate model index */
#define PERSISTENTIDX "PersistentIdx"

/*
 * Matlab gateway function hmmPersistentRegister()
 * See hmmPersistentRegister.m for documentation
 */

void
mexFunction(int nlhs, mxArray *plhs[],
	    int nrhs, const mxArray *prhs[])
{
  /* parameter positions  ----------------------------------------*/
  
  /* in */
  const int	InCount = 0;
  const int	OutCount = 0;
  
  int		ModelIndex;
  
  void		*soLib = NULL;
  HMMCache	*ModelCache;
  HMM		*HMMEntry;
  char		*error;
  char		ErrorString[MAXSTRING];
  mxArray	*MxPersistentIndex;

  GET_FN	get;

  if (nrhs < InCount || nrhs > InCount)
    mexErrMsgTxt("Invalid arguments");

  /* Open library */
  soLib = dlopen(UTSHAREDLIB, RTLD_LAZY);
  soThrowIfException(soLib);

  get = dlsym(soLib, "get");	/* get function handle */
  soThrowIfException(soLib);

  /* Retrieve pointer array */
  ModelCache = get(HMMSEGMENT);

  /* Close library */
  dlclose(soLib);

  /* Free any previously allocated models */
  if (ModelCache) {
    int	i;

    for (i=0; i < ModelCache->N; i++) {
      if (ModelCache->HMM[i]) {
	hmmFreeModelAccessor(ModelCache->HMM[i], ALLOC_CLIB);
	ModelCache->HMM[i] = (HMM *) 0;
      }
    }
  }
}
    

    
