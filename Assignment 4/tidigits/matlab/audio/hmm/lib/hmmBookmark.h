/*
 * hmmBookmark.h
 * Data structures and macros to support HMM probability computation.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#ifndef HMMBOOKMARK_H

#define HMMBOOKMARK_H	1


/* When evaluating models, we need to step through the mixtures.
 * This structure provides a convenient "bookmark" to encapsulate
 * the pointers to the current mixture parameters.
 */

typedef struct {
  int		State;
  int		Mixture;

  /*
   * Each of the following variables points to the appropriate value
   * in the model data structure.  The general layout of the data to
   * which the variable points is indicated in comments.
   */
  const double	* MeansPtr;		/* Mixtures X Components X State */
  const double	* MixtureWtsPtr;	/* Mixtures X States */
  const double	* pdf_kPtr;		/* Mixtures X States */
  const double	* SigmaInvPtr;		/* diagonal values VarCovar^-1 */

} HMMBookmark;

/* BM_SET_STATEMIX(Bookmark, ModelPtr, SetState, SetMixture)
 * Initialize a bookmark to a specific state and mixture.
 */
#define BM_SET_STATEMIX(Bookmark, ModelPtr, SetState, SetMixture) \
  (Bookmark).State = (SetState) ; \
  (Bookmark).Mixture = (SetMixture) ; \
  (Bookmark).MixtureWtsPtr = COL_ACCESS((ModelPtr)->MixtureWtsPtr, \
       (SetMixture), (SetState), (ModelPtr)->Mixtures); \
  (Bookmark).pdf_kPtr = \
       COL_ACCESS((ModelPtr)->pdf_kPtr, (SetMixture), (SetState), (ModelPtr)->Mixtures); \
  (Bookmark).MeansPtr = (ModelPtr)->MeansPtr + (SetMixture) + \
    (SetState) * (ModelPtr)->Components * (ModelPtr)->Mixtures; \
  (Bookmark).SigmaInvPtr = \
    * COL_ACCESS((ModelPtr)->SigmaInv, (SetMixture), (SetState), (ModelPtr)->Mixtures);

/* BM_SET_MIX(Bookmark, ModelPtr, SetMixutre)
 * Set bookmark pointer in an already initialized bookmark
 * to a specified mixture
 */

#define BM_SET_MIX(Bookmark, ModelPtr, SetMixture) \
  (Bookmark).Mixture = (SetMixture) ; \
  (Bookmark).MixtureWtsPtr = \
	COL_ACCESS_OFFSET((ModelPtr)->MixtureWtsPtr, (SetMixture)); \
  (Bookmark).pdf_kPtr = \
	COL_ACCESS_OFFSET((ModelPtr)->pdf_kPtr, (SetMixture)); \
  (Bookmark).MeansPtr = (ModelPtr)->MeansPtr + (SetMixture); \
  (Bookmark).SigmaInvPtr = \
	*COL_ACCESS_OFFSET((ModelPtr)->SigmaInv, (SetMixture));

/* Access specific mixtures - Assumes bookmark has been set to state s, mixture 0  */
#define BM_GETMIX_WEIGHT(ModelPtr, Mixture) \
	*(COL_ACCESS_OFFSET((ModelPtr)->MixtureWtsPtr, (Mixture)))
#define BM_GETMIX_PDF_KPTR(ModelPtr, Mixture) \
	COL_ACCESS_OFFSET((ModelPtr)->pdf_kPtr, (Mixture))
#define BM_GETMIX_MEANSPTR(ModelPtr, Mixture) \
	((ModelPtr)->MeansPtr + (Mixture))
#define BM_GETMIX_SIGMAINVPTR(ModelPtr, Mixture) \
	*COL_ACCESS_OFFSET((ModelPtr)->SigmaInv, (Mixture))

   
/* BM_SET_NEXTMIX(Bookmark, ModelPtr)
 * Set bookmark pointers in an already initialized bookmark
 * to the next mixture.  This macro is invalid when the bookmark
 * points to the last mixture in a state.
 */
#define BM_SET_NEXTMIX(Bookmark, ModelPtr) \
  (Bookmark).Mixture++; \
  COL_INCROW((Bookmark).MeansPtr, (ModelPtr)->Mixtures, (ModelPtr)->States); \
  COL_INCROW((Bookmark).MixtureWtsPtr, (ModelPtr)->Mixtures, (ModelPtr)->States); \
  COL_INCROW((Bookmark).pdf_kPtr, (ModelPtr)->Mixtures, (ModelPtr)->States); \
  (Bookmark).SigmaInvPtr = \
    * COL_ACCESS((ModelPtr)->SigmaInv, (Bookmark).Mixture, (Bookmark).State, (ModelPtr)->Mixtures);

#endif

