/*
 * hmmFundThmCalc.c
 * Perform integration via the fundamental theorem of calculus
 *
 * This code is copyrighted 2002 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */
#include <math.h>
#include <stdlib.h>
#include <mex.h>

#include "hmmlib.h"
#include "hmmBookmark.h"

#ifdef LIBM_NO_ERF
#include "fdlib/fdlibm.h"
#define erf s_erf
#endif


/*
 * double
 * hmmFundThmCalc(const int State, HMM *Model,
 *		  const double *Displacement,
 *		  const double *ObservationPtr
 *		  double *f_Mixtures)
 *
 * Return the probability across a region defined by the displacement
 * about an observation.  Uses the fundamental theorem of calculus to
 * compute the area under each dimension.
 *
 * from Maple:
 * # 1-d Gaussian
 * f1 := o -> 1 / sqrt(2*Pi*Sigma_1) * exp(-(1/2)*(o - mu_1)^2/(Sigma_1));
 *
 * simplify(integrate(f1(o), o=a..b));
 *	1/2*erf(1/2*sqrt(2)*(b-mu_1)/(sqrt(Sigma_1))) -
 *	1/2*erf(1/2*sqrt(2)*(a-mu_1)/(sqrt(Sigma_1)));
 *
 * Expects:
 *	State - state to evaluation
 *	Model - Model structure with fields initialized to point
 *		to state specific data the mixture weights and
 *		pdf constants.
 *	Displacement - Maximum displacement along each component
 *	ObservationPtr - Observed data.
 *	f_Mixtures - Output argument.  Matrix of state specific
 *		mixture probabilities
 * Returns:
 * probability of observation in state
 */

double
hmmFundThmCalc(const int State,
	    HMM *Model,
	    const double *Displacement,
	    const double *ObservationPtr,
	    double *f_Mixtures)
{

  /* Note, we don't actually use mu as the integral limits have mu
   * subtracted from them, leaving the displacement.  We'll still pass
   * mu as it doesn't cost much to pass a pointer and looks strange
   * without it.
   */

  double	f_State, f_Mix;		/* state & mixture probabilities */
  HMMBookmark	Bookmark;
  int		c;
  int		Mixture;
  double	HalfSqrt2 = 0.5 * sqrt(2);
  const double	*PrecisionPtr;
  double	SqrtPrecision;

  f_State = 0.0;
  
  /* Set pointers to the  appropriate distribution */
  BM_SET_STATEMIX(Bookmark, Model, State, 0);

  for (Mixture = 0; Mixture < Model->Mixtures; Mixture++) {

    f_Mix = 1.0;
    PrecisionPtr = Bookmark.SigmaInvPtr;
    for (c=0; c < Model->Components; c++) {
      /* evaluate integral */
      SqrtPrecision = sqrt(*PrecisionPtr);
      PrecisionPtr++;
      f_Mix *= 0.5 *
	(erf(HalfSqrt2 * Displacement[c] * SqrtPrecision)
	 - erf(HalfSqrt2 * -Displacement[c] * SqrtPrecision));
    }

    f_Mixtures[Mixture] += (* Bookmark.MixtureWtsPtr) * f_Mix;
    
    BM_SET_NEXTMIX(Bookmark, Model);	/* Point to next mixture */
  }

  /* Sum the mixtures and return */
  for (Mixture = 0; Mixture < Model->Mixtures; Mixture++) {
    f_State += f_Mixtures[Mixture];

    /* Check for underflow to avoid log 0 problems
     * Note that this could cause the sum of mixtures to differ
     * slightly from the state probability
     */
    if (! f_Mixtures[Mixture])
      f_Mixtures[Mixture] = MinIEEE;
  }

  /* Check for underflow to avoid log 0 problems */
  if (! f_State)
    f_State = MinIEEE;	/* warning:  State != sum mixtures */
  
# if DEBUG
  printf("\nSum=%f\n", f_State);
# endif

  return f_State;
}
  

