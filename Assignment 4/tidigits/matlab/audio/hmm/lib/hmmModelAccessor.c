/*
 * hmmModelAccessor.c
 *
 * This code is copyrighted 2001-2003 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#include <mex.h>
#include <matrix.h>
#include <stdarg.h>

#include "hmmlib.h"
#include "hmmModelAccessor.h"
#include "soException.h"

#ifdef UNIX
#include <dlfcn.h>
#include <utSharedLib.h>
#endif

#include <MxUtil.h>


/* MEM_ALLOC(Size, Type) - Allocation of Size bytes.  The allocation
 * routines invoked depend on Type: 
 * ALLOC_MATLAB - Register memory with Matlab
 * ALLOC_CLIB - Standard C libraries
 */
#define MEM_ALLOC(Size, Type) \
	(((Type) == ALLOC_MATLAB) ? \
	  mxMalloc((Size)) : malloc((Size)))

/* MEM_FREE(Pointer, Type) - Free previously allocated memory
 * Deallocation routines invoked depend on Type:
 * ALLOC_MATLAB - Matlab deallocation routines
 * ALLOC_CLIB - Standard C libraries
 */
#define MEM_FREE(Pointer, Type) \
	(((Type) == ALLOC_MATLAB) ? \
	  mxFree((Pointer)) : free((Pointer)))

/*
 * HMMCache * hmmCacheAccessor(mxArray const *MxAnyHMM)
 *
 * Given a pointer to any HMM which has been marked persistent,
 * return the pointer to the HMM cache.  The value NULL indicates
 * that the model was not cached.  Exceptions will be thrown when
 * the model should have been cached, but we were unable to access
 * the cache.
 */
HMMCache *
hmmCacheAccessor(mxArray const *MxAnyHMM) {
  int		Index;
  HMMCache	*Cache;
  char		Error[MAXSTRING];

  void		*soLib = NULL;
#ifdef UNIX
  GET_FN	get;
#endif
  char		*error;

#ifdef UNIX
  Index = hmmModelPersistentIndexAccessor(MxAnyHMM);
  if (Index > -1) {
    /* HMM has been marked persistent.  Attempt to open library */

    soLib = dlopen(UTSHAREDLIB, RTLD_LAZY);
    if ((error = dlerror())) {
      /* unable to open */
      sprintf(Error, "Unable to open: %s", error);
      /* die - we could of course return NULL, but this should
       * not occur and the user may want to know about it.
       */
      soThrowException(soLib, Error);
    } else {
      get = dlsym(soLib, "get");	/* bind function handle */
      if ((error = dlerror())) {
	/* unable to access fn */
	soThrowException(soLib, "Unable to access function handle");
      } else {
	/* shared object library all set, let's go! */
	Cache = (HMMCache *) get(HMMSEGMENT);
	if (! Cache)
	  soThrowException(soLib, "Null HMM cache.  Were models registered?");
	dlclose(soLib);
      }
    }
  } else
#endif
    Cache = NULL;

  return Cache;

}

/*
 * int hmmModelPersistentIndexAccessor(mxArray const *MxHMM) 
 *
 * Returns the persistent index of a model which is used for caching.
 * If there is no persistent index, returns -1.
 */
int
hmmModelPersistentIndexAccessor(mxArray const *MxHMM) {
  mxArray	*MxField;
  int		Index;

  MxField = mxGetField(MxHMM, 0, "PersistentIdx");
  if (MxField) 
    Index = mxGetScalar(MxField);
  else
    Index = -1;

  return Index;
}

/*
 * hmmModelAccessorCached(HMM *DestHMM, HMMCache *Cache, int Index)
 * Populate the destination HMM based upon the Index'th HMM located
 * in the HMMCache structure.
 */
void
hmmModelAccessorCached(HMM *DestHMM, HMMCache *Cache, int Index) {
  HMM		*Model;
  char		Error[MAXSTRING];

  /* Check bounds */
  if (Index < 0 || Index >= Cache->N) {
    sprintf(Error, "Index = %d out of range [0,%d]", Index, Cache->N);
    mexErrMsgTxt(Error);
  }
  
  Model = Cache->HMM[Index];	/* Grab pointer to model */

  /* copy information */
  DestHMM->States = Model->States;
  DestHMM->Mixtures = Model->Mixtures;
  DestHMM->Components = Model->Components;
  DestHMM->PiPtr = Model->PiPtr;
  DestHMM->APtr = Model->APtr;
  DestHMM->MixtureWtsPtr = Model->MixtureWtsPtr;
  DestHMM->MeansPtr = Model->MeansPtr;
  DestHMM->pdf_kPtr = Model->pdf_kPtr;
  DestHMM->pdf_logkPtr = Model->pdf_logkPtr;
  DestHMM->Sigma = Model->Sigma;
  DestHMM->SigmaInv = Model->SigmaInv;

  DestHMM->AllocationType = ALLOC_RETRIEVE;
}	  

  

/*
 * hmmModelAccessor(mxArray *MxSourceHMM, HMM *DestHMM,
 *	ALLOC_METHOD AllocMethod)
 * Expects pointers to a populated Matlab HMM model and an empty
 * C HMM structure.  Populates the C structure from the Matlab one.
 * AllocMethod indicates whether C lib or Matlab allocation functions
 * should be used.  Matlab allocation routines automatically deallocate
 * after a Mex call completes.
 *
 * When hmmModelAccessor is called with allocation method
 * ALLOC_RETRIEVE, a shared library is accessed to retrieve the model
 * indicated by the field PersistentIdx.  If the retrieval fails, a
 * warning is generated and the call proceeds with allocation type
 * ALLOC_MATLAB.
 *
 * CAVEATS:  This routine is designed to be called on automatically
 * generated Matlab HMMs.  Although some error checking is done
 * (i.e. to make sure fields exist before accessing them), error
 * checking is minimal and ill-formed models may result in
 * severe problems.
 */
void
hmmModelAccessor(mxArray const *MxSourceHMM, HMM *DestHMM,
		 ALLOC_METHOD AllocMethod)
{
  mxArray	*MxMixInfo;
  mxArray	*MxCovInfo;
  mxArray	*MxMeans;
  mxArray	*MxSigmaInv, *MxSigmaInvCells;
  mxArray	*MxSigma, *MxSigmaCells;
  mxArray	*MxField;

  double	**PrecisionPtr, **CovariancePtr;
  int		State, Mix;
  char		Error[MAXSTRING];


  if (AllocMethod == ALLOC_RETRIEVE) {
    HMMCache	*Cache;
    int		Index;

    Index = hmmModelPersistentIndexAccessor(MxSourceHMM);
    if (Index > -1) {
      /* caller attempting to retrieve a previously cached model */
      Cache = hmmCacheAccessor(MxSourceHMM);
      if (Cache)
	hmmModelAccessorCached(DestHMM, Cache, Index);
      else {
	/* Persistent models not available although they
	 * should have been.  Warn and pursue alternate
	 * strategy for retrieval.
	 */
	sprintf(Error, "Unable to retrieve persistent HMM %d\n", Index);
	mexWarnMsgTxt(Error);
	AllocMethod = ALLOC_MATLAB;
      }
    } else {
      /* Index shows this model was not cached */
      AllocMethod = ALLOC_MATLAB;
    }
  }
	
  DestHMM->AllocationType = AllocMethod;

  if (DestHMM->AllocationType != ALLOC_RETRIEVE) {
  
    MxAccessField(MxSourceHMM, MxField, "NumberStates", Error);
    DestHMM->States = (int) mxGetScalar(MxField);
    
    MxAccessField(MxSourceHMM, MxField, "NumberMixtures", Error);
    DestHMM->Mixtures = (int) mxGetScalar(MxField);
    
    MxAccessField(MxSourceHMM, MxField, "Components", Error);
    DestHMM->Components = (int) mxGetScalar(MxField);

    MxAccessField(MxSourceHMM, MxField, "pi", Error);
    DestHMM->PiPtr = mxGetPr(MxField);
  
    MxAccessField(MxSourceHMM, MxField, "a", Error);
    DestHMM->APtr = mxGetPr(MxField);
  
    MxAccessField(MxSourceHMM, MxMixInfo, "Mix", Error);
    MxAccessField(MxMixInfo, MxCovInfo, "cov", Error);
    MxAccessField(MxCovInfo, MxSigmaInvCells, "SigmaInv", Error);
    MxAccessField(MxCovInfo, MxSigmaCells, "Sigma", Error);

    /* Populate mixture means, weights, constants */
    MxAccessField(MxMixInfo, MxMeans, "mu", Error);
    DestHMM->MeansPtr = mxGetPr(MxMeans);

    MxAccessField(MxMixInfo, MxField, "c", Error);
    DestHMM->MixtureWtsPtr = mxGetPr(MxField);

    MxAccessField(MxCovInfo, MxField, "k", Error);
    DestHMM->pdf_kPtr = mxGetPr(MxField);

    MxAccessField(MxCovInfo, MxField, "logk", Error);
    DestHMM->pdf_logkPtr = mxGetPr(MxField);

    /* Populate the variance/covariance matrices and their inverses.
     *
     * To avoid propogating Matlab Mx interface calls in other parts
     * of the library, we will allocate an array of pointers to doubles
     * and populate them to point to the real part of the diagonal
     * variance/covariance matrices.
     */
  
    if (! (DestHMM->Sigma = (double **)
	   MEM_ALLOC(DestHMM->Mixtures * DestHMM->States * sizeof(double **),
		     DestHMM->AllocationType))) {
      mexErrMsgTxt("Unable to allocate covariance pointers in hmmModelAccessor");
    }
    if (! (DestHMM->SigmaInv = (double **)
	   MEM_ALLOC(DestHMM->Mixtures * DestHMM->States * sizeof(double **),
		     DestHMM->AllocationType))) {
      mexErrMsgTxt("Unable to allocate precision pointers in hmmModelAccessor");
    }

    PrecisionPtr = DestHMM->SigmaInv;
    CovariancePtr = DestHMM->Sigma;
  
    for (State = 0; State < DestHMM->States; State++) {
      for (Mix = 0; Mix < DestHMM->Mixtures; Mix++) {

	MxSigmaInv = mxGetCell(MxSigmaInvCells,
			       COL_MAJOR(Mix, State, DestHMM->Mixtures));
	MxSigma = mxGetCell(MxSigmaCells,
			    COL_MAJOR(Mix, State, DestHMM->Mixtures));
	/* Verify that inverted variance/covariance matrices are sparse
	 * This is performed up front so integration methods do not
	 * check multiple times.
	 */
	if ((! mxIsSparse(MxSigmaInv)) || (! mxIsSparse(MxSigma))) {
	  mxFree(DestHMM->SigmaInv);
	  mxFree(DestHMM->Sigma);
	  mexErrMsgTxt("Model's covariance & precision matrices must be sparse");
	}

	*PrecisionPtr = mxGetPr(MxSigmaInv);
	PrecisionPtr++;

	*CovariancePtr = mxGetPr(MxSigma);
	CovariancePtr++;
      }
    }
  }
}


/*
 * hmmFreeModelAccessor(HMM *, ALLOC_METHOD)
 * Release memory associated with a model.
 *
 * CAVEATS:
 * Release is only done if the allocation type matches
 * the one used in model creation.  This permits
 * common code branches to be executed regardless of the
 * allocation method used.
 */
void 
hmmFreeModelAccessor(HMM *Model, ALLOC_METHOD AllocMethod)
{
  /* Only free if method matches allocation type */
  if (AllocMethod == Model->AllocationType) {
    if (Model->SigmaInv)
      MEM_FREE(Model->SigmaInv, Model->AllocationType);
    if (Model->Sigma)
      MEM_FREE(Model->Sigma, Model->AllocationType);
  }
}
