/*
 * hmmDebug.h
 * macros to support HMM debugging.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#ifndef HMMDEBUG_H
#define HMMDEBUG_H	1

/* If file including this header has set debug, permit it
 * to override the global value.  Useful for debugging sections of code.
 */
#ifndef DEBUG
#define DEBUG 0		/* 0 = no debug, otherwise = debug */
#endif

#if DEBUG

#include <stdio.h>
#include "hmmBookmark.h"

/* function prototypes */
void hmmDebugBookmark(HMMBookmark *Bookmark, HMM *Model);
void hmmDebugVector(const double *Vector, int Components);

#else
/* no debug - define empty macros */
#define hmmDebugBookmark(Bookmark, Model)
#define hmmDebugVector(Vector, Components)

#endif

#endif
