/*
 * hmmXi.c
 *
 * Compute expectations for state transitions.
 * Rabiner/Juang note this as
 *	Xi_t(i,j) = P(q_t = i, q_{t+1}=j, O | Model)
 *		    --------------------------------
 *			     P(O | Model)
 * For the maximization step, we only need the sum of
 * the Xi_t matrices over time, so we will only return
 * Xi(i,j) = sum_{t=1}^T Xi_t(i,j)
 * 
 * We will assume independendence between components.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#include "hmmlib.h"
#include <mex.h>
#include <matrix.h>

/*
 * hmmXi - Part of the EM algorithm for HMMs (Baum-Welch reestimation
 * forumulas)
 *
 * Computes the Xi matrix.
 * Xi[i][j] = Expected number of transitions from state i to state j
 *	      with respect to the observed data.
 */

void hmmXi(double *Xi,
	   double *Alpha, double *Beta,
	   HMM *Model, double *Obs, int T)
{
  double	*Xi_t;		/* Pr(q_t=i, q_{t+1}=j, Model) */
  double	*Obs_t;
  double	PrO;		/* Pr(o_t = o | q_t=i, q_{t+1}=j, Model) */
  double	*PrMixture_ti;
  int		t;
  int		sFrom, sTo;
  double	Alphat, Betat, Aij, b;

  /* Probability of mixtures for a given state & time */
  PrMixture_ti = mxMalloc(Model->Mixtures * sizeof(double));

  /* Allocate Xi_t for a specific time t */
  if (! (Xi_t = mxMalloc(Model->States * Model->States * sizeof(double))))
    mexErrMsgTxt("Unable to allocate Xi_t");

  Obs_t = Obs;
  COL_INCCOL(Obs_t, Model->Components, T);	/* Next observation */
  for (t = 0; t < T - 1; t++) {

    PrO = 0;
    for (sFrom = 0; sFrom < Model->States; sFrom++) {
      for (sTo = 0; sTo < Model->States; sTo++) {
	/*
	 * Xi_t = Pr of seeting Obs[t] at q_t=sFrom, q_{t+1}=sTo
	 *	=      P(q_t = i, q_{t+1}=j, O | Model)
	 *	  ----------------------------------------
	 *		P(O|Model)
	 *
	 *	=      alpha_t(i) a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)
	 *	  --------------------------------------------------------
	 *	  sum_i sum_j alpha_t(i) a_{i,j} b_j(o_{t+1}) beta_{t+1}(j)
	 *
	 * Consider the sum of the rows & columns of Xi:
	 *	sum_i sum_j Xi_t(i, j)
	 * This is simply the probability of the observation sequence:
	 *	Pr(O|Model) = sum_i sum_j Xi_t(i, j)
	 * since we are considering every possible state transition
	 * at time t, t+1.
	 *
	 */

	Alphat = *COL_ACCESS(Alpha, t, sFrom, T);
	Betat = *COL_ACCESS(Beta, t+1, sTo, T);
	Aij = *COL_ACCESS(Model->APtr, sFrom, sTo, Model->States);
	b = hmm_f_state(sFrom, Model, PrMixture_ti, Obs_t);
	  
	*ROW_ACCESS(Xi_t, sFrom, sTo, Model->States) =
	  *COL_ACCESS(Alpha, t, sFrom, T) *
	  *COL_ACCESS(Model->APtr, sFrom, sTo, Model->States) *
	  hmm_f_state(sFrom, Model, PrMixture_ti, Obs_t) *
	  *COL_ACCESS(Beta, t+1, sTo, T);

	*ROW_ACCESS(Xi_t, sFrom, sTo, Model->States) =
	  Alphat * Aij * b * Betat;
	
	/* Track the Pr(O) to make sure that it really is equal to 1.
	 * If the user isn't using scaling at each step, our scheme
	 * falls apart.
	 */
	PrO += *ROW_ACCESS(Xi_t, sFrom, sTo, Model->States);
	
      }
    }

    for (sFrom = 0; sFrom < Model->States; sFrom++) {
      for (sTo = 0; sTo < Model->States; sTo++) {

	/* Normalize Xi */
	*ROW_ACCESS(Xi_t, sFrom, sTo, Model->States) /=
	  PrO;
	
	/* Xi = sum over time Xi_t */
	*ROW_ACCESS(Xi, sFrom, sTo, Model->States)
	  += *ROW_ACCESS(Xi_t, sFrom, sTo, Model->States);
      }
    }

    COL_INCCOL(Obs_t, Model->Components, T);	/* Next observation */
  }

  /* Release requested resources */
  if (Xi_t)
    mxFree(Xi_t);
}

  



  
