/*
 * hmmCubature.c
 * Perform integral approximation via cubature methods.
 *
 * This code is copyrighted 1997-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */
#include <math.h>
#include <stdlib.h>
#include <mex.h>

#include "hmmlib.h"
#include "hmmBookmark.h"
#include "hmmCubature.h"

#include "hmmDebug.h"

/*
 * hmmCubatureInit(CubatureInfo *, const BasisSet *, int Evaluations)
 * Some of the tasks related to performing the cubature approximation
 * to the integral can be precomputed, thus preventing multiple
 * 
 *
 */
void
hmmCubatureInit(CubatureInfo *Info,
		const BasisSet *Basis,
		const double *Displacement,
		int Evaluations)
{
  int	b, c;
  
  /* The quadrature method uses dynamic nested loops.
   * Construct a table with the offset multiple for each dimension.
   */
  Info->LoopThresh[0] = Evaluations;
  for (b = 1; b < Basis->BasisCount; b++)
    Info->LoopThresh[b] = Evaluations * Info->LoopThresh[b - 1];
  /* Total number points to evaluate */
  Info->Samples = Info->LoopThresh[Basis->BasisCount - 1];

  /* Determine the incremental offset in the direction of each basis
   * Treat as a column-major matrix to avoid confusion with other
   * column-major data structures.	Basis x Components
   */
  Info->Delta = mxMalloc(Basis->BasisCount * Basis->Components *
		       sizeof(double));
  for (b = 0; b < Basis->BasisCount; b++) {
    for (c = 0; c < Basis->Components; c++) {
      *COL_ACCESS(Info->Delta, c, b, Basis->Components) =
	2 * Displacement[b] / (Evaluations - 1) *
	*COL_ACCESS(Basis->Basis, c, b, Basis->Components);
    }
  }
}

void
hmmCubatureRelease(CubatureInfo *Info)
{
  mxFree(Info->Delta);
}

/*
 * double
 * hmmCubature(const int State, HMM *Model,
 *	const BasisSet *Basis, const CubatureInfo *Cubature
 *	const double *Displacement, const double *ObservationPtr
 *	double *f_Mixtures)
 *
 * Return the probability for a specific state when smoothed via a
 * simple cubature based integration (no weighting) in a region about
 * the current observation.  In this simplified approximation of an
 * integral, we assume that the sample points are uniformly
 * distributed and do not multiply by the volume of the manifold.
 *
 * This implements the following function:
 *
 *  Pr(o_t | M) ~= sum_{i(1)=1}^E ... sum_{i(B)=1}^E  f(o_t + step(i))
 *
 * where:
 *
 * step(i) = sum_{j=1}^B (-k_j + (i_j - 1)*(2 k_j / (E-1))) * phi_j
 *		phi_j is j'th basis vector
 *		B = cardinality of basis vector set
 *		k_j = maximum displacement along j'th basis vector
 *		E = number of evaluations
 * Expects:
 *	State - state to evaluation
 *	Model - Model structure with fields initialized to point
 *		to state specific data the mixture weights and
 *		pdf constants.
 *	BasisSet - Set of basis vectors defining integration space directions
 *	Displacement - Maximum displacement in each basis direction.
 *	ObservationPtr - Observed data.
 *	f_Mixtures - Output argument.  Matrix of state specific
 *		mixture probabilities
 * Returns:
 * probability of observation in state */

double
hmmCubature(const int State,
	    HMM *Model,
	    const BasisSet *Basis,
	    const CubatureInfo *Cubature,
	    const double *Displacement,
	    const double *ObservationPtr,
	    double *f_Mixtures)
{
  double	ObsPlusEps[MAXCOMPONENTS];
  HMMBookmark	Bookmark;
  
  double	f_State, f_Mix;		/* state & mixture probabilities */
  int		Mixture;
  int		CurrentBasis;

  int		b, c, pdfEvals, Idx;


  /* ObsPlusEps = first point to be evaluated */
  /* copy observation */
  for (c=0; c < Model->Components; c++)
    ObsPlusEps[c] = ObservationPtr[c];
  /* ObsPlusEps = Obs - each basis vector scaled by the displacement */
  for (c=0; c < Model->Components; c++) {
    for (b=0; b < Basis->BasisCount; b++) {
      ObsPlusEps[c] -=
	Displacement[b] * *COL_ACCESS(Basis->Basis, c, b, Model->Components);
    }
  }

  /* init probabilities */
  f_State = 0.0;
  for (Idx = 0; Idx < Model->Mixtures; Idx++) {
    f_Mixtures[Idx] = 0.0;
  }

  /*
   * Integrate across the region defined by the basis set and displacment
   *
   * In effect, we want the following loop
   * for b1 = 1 to E (where E = number of evaluations along each bases)
   *    for b2 = 1 to E
   *	    ...
   *	       for bp = 1 to E
   *	          compute probability of obs + an offset consisting of a
   *		   linear combination of the the basis vectors.  To
   *		   speed up obs + offset computation, we maintain a
   *		   running sum (which may lead to roundoff errors)
   *		   which is added/subtracted to as needed.
   *
   * Since the number of bases is unknown, we dynamically simulate
   * nested loops using a single loop and an array of counters
   * Basis->LoopThresh which was initialized prior to calling this
   * routine:
   *
   *	LoopThresh[i]= E^(i+1)
   *
   * At the bottom of the loop, we check to see if E divides the current
   * loop index evenly.  If so, we are at the boundary of one of our
   * loops, and check to see which ones need to be reset.  
   */
  Idx = 1;
  CurrentBasis = 0;	/* current level virtual loop 0=innermost */
  while (Idx <= Cubature->Samples) {

    /* Set pointers to the  appropriate distribution */
    BM_SET_STATEMIX(Bookmark, Model, State, 0);

#   if DEBUG
    printf("\nIdx=%d ", Idx);
    hmmDebugVector(ObsPlusEps, Model->Components);
#   endif
    /* Evalutate each mixture for the current state */
    for (Mixture = 0; Mixture < Model->Mixtures; Mixture++) {
      
      f_Mix = /* evaluate pdf */
	hmm_f_Gaussian_ind(Bookmark.pdf_kPtr,
		       Bookmark.MeansPtr,
		       Bookmark.SigmaInvPtr,
		       ObsPlusEps,
		       Model->Components,
		       Model->Mixtures);

#     if DEBUG
      printf("m%d=%f ", Mixture, f_Mix);
#     endif    

      f_Mixtures[Mixture] += (* Bookmark.MixtureWtsPtr) * f_Mix;
      
      BM_SET_NEXTMIX(Bookmark, Model);	/* Point to next mixture */
    }

    /*
     * Check to see if we have reached the end of any of the
     * virtual nested loops and handle appropriately.
     */
    while (! (Idx % Cubature->LoopThresh[CurrentBasis]) &&
	   Idx < Cubature->Samples) {
      /* Completed all operations in direction CurrentBasis.
       * Subtract off the distance we have moved along this direction.
       * Note that this method is prone to rounding errors, but suffices
       * for the rough approximation we are using.
       */
      for (c=0; c < Model->Components; c++) 
	ObsPlusEps[c] -= 2.0 * Displacement[CurrentBasis] *
	  *COL_ACCESS(Basis->Basis, c, CurrentBasis, Model->Components);
      CurrentBasis++;	/* Move to next layer out */
    }

    Idx++;

    /* CurrentBasis now contains the innermost loop which has not completed.
     * Increment in the direction of the associated basis vector.
     */
    for (c=0; c < Model->Components; c++)
      ObsPlusEps[c] +=
	*COL_ACCESS(Cubature->Delta, c, CurrentBasis, Basis->Components);

    /* Now that we have performed the action associated with the
     * the innermost loop which has not completed, we need to
     * return to the innermost loop:
     */
    CurrentBasis = 0;
    
  } /* end while Idx */

  /* Sum the mixtures and return */
  for (Idx = 0; Idx < Model->Mixtures; Idx++) {
    f_State += f_Mixtures[Idx];

    /* Check for underflow to avoid log 0 problems
     * Note that this could cause the sum of mixtures to differ
     * slightly from the state probability
     */
    if (! f_Mixtures[Idx])
      f_Mixtures[Idx] = MinIEEE;
  }

  /* Check for underflow to avoid log 0 problems */
  if (! f_State)
    f_State = MinIEEE;	/* warning:  State != sum mixtures */
  
# if DEBUG
  printf("\nSum=%f\n", f_State);
# endif

  return f_State;
}

  

