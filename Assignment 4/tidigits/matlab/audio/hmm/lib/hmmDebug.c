/*
 * hmmDebug.c
 * code to support HMM debugging.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#include <mex.h>	/* Let mex define printf to use Matlab's stdout */
#include "hmmlib.h"
#include "hmmBookmark.h"


/* Warning - do not include hmmDebug.h
 * When debugging is turned off, null macros are created
 * for the functions in this file which will give the compiler
 * a bad case of indigestion if included here.
 */
   


void hmmDebugBookmark(HMMBookmark *Bookmark, HMM *Model)
{
  int		i;
  double	*VarCovarInv;

  printf("Bookmark\nstate = %d, mixture = %d\n", Bookmark->State, Bookmark->Mixture);
  printf("\tweight=%f k=%f\n,u=",
	 *Bookmark->MixtureWtsPtr, *Bookmark->pdf_kPtr);
  
  for (i = 0; i < Model->Components; i++) {
    printf(" %f", *(Bookmark->MeansPtr + i * Model->Mixtures));
  }

  printf("\nvarcovar=");
  for (i = 0; i < Model->Components; i++) {
    printf(" %f", Bookmark->SigmaInvPtr[i]);
  }
  printf("\n");
}

void hmmDebugVector(const double *Vector, int Components)
{
  int	c;

  printf("vec: [");
  for (c = 0; c < Components; c++)
    printf("%f ", Vector[c]);
  printf("]\n");
}
