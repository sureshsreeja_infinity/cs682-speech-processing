void soThrowWarning(void *soLib, char *);
void soThrowException(void *soLib, char *);
char * soExceptionCheck(void);
void soThrowIfException(void *);

