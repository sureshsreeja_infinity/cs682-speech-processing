#include <mex.h>
#include <math.h>
#include "hmmlib.h"

double hmmViterbi(HMM *ModelPtr, double *StatePrPtr,
		  int T, double *OptStateSeqPtr)
{
  double	*LogA;
  double	*PrCurrent, *PrPrev, *PrTmp;
  double	*StateBackTrack;
  double	Pr, MaxProb, MaxState, LogProbability;
  double	Infinity, NegativeInfinity;
  int		i, j, t;
  int		StatesSquared;
  
  StatesSquared = ModelPtr->States * ModelPtr->States;

  LogA = mxMalloc(sizeof(double) * StatesSquared);
  PrCurrent = mxMalloc(sizeof(double) * ModelPtr->States);
  PrPrev = mxMalloc(sizeof(double) * ModelPtr->States);
  StateBackTrack = mxMalloc(sizeof(double) * T * ModelPtr->States);
  

  Infinity = mxGetInf();
  NegativeInfinity = 0 - Infinity;

  if (LogA && PrCurrent && PrPrev && StateBackTrack) {
    /* sucessfully allocated resources */
    
    /* build log state transition table */
    for (i=0; i < StatesSquared; i++) {
      LogA[i] = log(ModelPtr->APtr[i]);
    }

    /* initialize - log probability of being in initial state
     * and observing the first symbol
     */
    t = 0;
    for (i=0; i < ModelPtr->States; i++) {
      PrCurrent[i] = log(ModelPtr->PiPtr[i]) +
	log(*COL_ACCESS(StatePrPtr, i, t, ModelPtr->States));
    }

    /* recursive formulas - compute log likelihood at time
     * t in terms of previous likelihood
     */
    t = t + 1;
    while (t < T) {

      /* Make the current probabality estimate the previous one
       * by swapping the current and previous probability arrays.
       */
      PrTmp = PrPrev;
      PrPrev = PrCurrent;
      PrCurrent = PrTmp;

      /* for each state j, find the best path into j at time t */
      for (j=0; j < ModelPtr->States; j++) {
	/* find the state i at time t-1 which will produce the
	 * best path into state j at time t
	 */
       	MaxProb = NegativeInfinity;
	for (i=0; i < ModelPtr->States; i++) {
	  Pr = PrPrev[i] + *COL_ACCESS(LogA, i, j, ModelPtr->States);
	  if (Pr > MaxProb) {
	    MaxProb = Pr;
	    MaxState = i;
	  }
	}

	/* Best i now in MaxState - save it */
	*COL_ACCESS(StateBackTrack, t, j, T) = MaxState;

	/* Maxprob accounts for the probability of transitioning
	 * into j, but does not include the probability of the
	 * observation at time t.  Find probability of best path
	 * into j including the observation.
	 */
	PrCurrent[j] = MaxProb +
	  log(*COL_ACCESS(StatePrPtr, j, t, ModelPtr->States));
      }

      /* We now know the best path into each state at time t */
      t = t + 1;
    }

    /* termination */

    /* Find which state at time T produced the maximum likelihood */
    MaxProb = NegativeInfinity;
    for (j=0; j < ModelPtr->States; j++) {
      if (PrCurrent[j] > MaxProb) {
	MaxProb = PrCurrent[j];
	MaxState = j;
      }
    }

    LogProbability = MaxProb;

    if (OptStateSeqPtr) {
      /* caller wants best state path */
      t = T - 1;
      OptStateSeqPtr[t] = MaxState;
      while (t > 0) {
	OptStateSeqPtr[t-1] =
	  *COL_ACCESS(StateBackTrack, t, (int) OptStateSeqPtr[t], T);
	t = t - 1;
      }
    }
    
  } else {
    /* unable to allocate log storage, inf signals error */
    LogProbability =  Infinity;
  }

  /* release resources if successfully allocated */
  if (LogA)
    mxFree(LogA);
  if (PrCurrent)
    mxFree(PrCurrent);
  if (PrPrev)
    mxFree(PrPrev);
  if (StateBackTrack)
    mxFree(StateBackTrack);

  return(LogProbability);
}






