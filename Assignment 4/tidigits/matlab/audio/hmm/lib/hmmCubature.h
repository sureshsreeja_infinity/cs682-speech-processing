
/*
 * hmmCubature.h
 * Perform integral approximation via cubature methods.
 *
 * This code is copyrighted 1997-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#ifndef HMMCUBATURE_H

#define HMMCUBATURE_H	1

typedef struct {
  int LoopThresh[MAXCOMPONENTS];  /* Counters for dynamic nesting of loops */
  int Samples;		/* Number of points used to approximate integral */
  double *Delta;	/* Offsets */
  double MinIEEE;
} CubatureInfo;

#endif
