/*
 * hmm_f_State.c
 * Evaluate all mixtures in a specific state.
 *
 * This code is copyrighted 1997-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

#include "hmmlib.h"
#include "hmmBookmark.h"
#include "hmmDebug.h"

/*
 * hmm_f_state
 * Compute all mixtures associated with a state
 */
double
hmm_f_state(const int State,
	    HMM *Model,
	    double *f_Mixtures,
	    const double *ObservationPtr)
{
  HMMBookmark	Bookmark;

  double	f_State, f_Mix;
  int		Mixture;

  f_State = 0.0;
  
  /* Set pointers to the  appropriate distribution */
  Mixture = 0;
  BM_SET_STATEMIX(Bookmark, Model, State, Mixture);

  /* Evalutate each mixture for the current state */
  for (; Mixture < Model->Mixtures; Mixture++) {

    /* output params if debugging enabled. */
    /* hmmDebugBookmark(&Bookmark, Model); -- too much detail */
    
    f_Mix = /* evaluate pdf */
      hmm_f_Gaussian_ind(Bookmark.pdf_kPtr,
			 Bookmark.MeansPtr,
			 Bookmark.SigmaInvPtr,
			 ObservationPtr,
			 Model->Components,
			 Model->Mixtures);
    f_Mixtures[Mixture] = (* Bookmark.MixtureWtsPtr) * f_Mix;
    f_State += f_Mixtures[Mixture];
    
    /* Check for underflow to avoid log 0 problems
     * Note that this could cause the sum of mixtures to differ
     * slightly from the state probability
     */
    if (! f_Mixtures[Mixture])
      f_Mixtures[Mixture] = MinIEEE;
    
    BM_SET_NEXTMIX(Bookmark, Model);	/* Point to next mixture */
  }

  /* Check for underflow to avoid log 0 problems */
  if (! f_State)
    f_State = MinIEEE;	/* warning:  State != sum mixtures */
  
  return f_State;
}


/*
 * hmm_f_state
 * Compute specific mixtures associated with a state
 * Mainly a copy of hmm_f_state with logic to only
 * evaluate specific mixtures.  Implemented separately
 * as opposed to an if statement for speed.
 */
double
hmm_f_state_mixture_list(const int State,
			 HMM *Model,
			 double *f_Mixtures,
			 const double *ObservationPtr,
			 int  MixtureListSize,
			 const double *MixtureListPtr)
{
  HMMBookmark	Bookmark;

  double	f_State, f_Mix;
  int		Mixture;
  int		NextMixture;

  f_State = 0.0;	/* state probability */

  /* Set to first mixture to use (-1 due to Matlab/C difference) */
  NextMixture = (int) *MixtureListPtr - 1;

  /* Set pointers to the  appropriate distribution */
  Mixture = 0;
  BM_SET_STATEMIX(Bookmark, Model, State, Mixture);

  /* As long as we have not processed all the mixtures
   * in the state or stepped past the end of the mixture list...
   */
  while (MixtureListSize > 0 && Mixture < Model->Mixtures) {

    if (Mixture == NextMixture) {
      /* mixture is on list to evaluate */
      
      /* output params if debugging enabled. */
      /* hmmDebugBookmark(&Bookmark, Model); -- too much detail */
      
      f_Mix = /* evaluate pdf */
	hmm_f_Gaussian_ind(Bookmark.pdf_kPtr,
			   Bookmark.MeansPtr,
			   Bookmark.SigmaInvPtr,
			   ObservationPtr,
			   Model->Components,
			   Model->Mixtures);
      f_Mixtures[Mixture] = (* Bookmark.MixtureWtsPtr) * f_Mix;
      f_State += f_Mixtures[Mixture];
      
      /* Check for underflow to avoid log 0 problems
       * Note that this could cause the sum of mixtures to differ
       * slightly from the state probability
       */
      if (! f_Mixtures[Mixture])
	f_Mixtures[Mixture] = MinIEEE;

      /* Set up to handle the next mixture on the list */
      NextMixture = (int) *(++MixtureListPtr) - 1;
	 
    }
    
    BM_SET_NEXTMIX(Bookmark, Model);	/* Point to next mixture */
    Mixture++;
  }

  /* Check for underflow to avoid log 0 problems */
  if (! f_State)
    f_State = MinIEEE;	/* warning:  State != sum mixtures */
  
  return f_State;
}
