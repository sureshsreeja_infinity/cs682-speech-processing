function AdaptedModel = hmmBayesAdapt(Model, AdaptationData)
% AdatptedState = hmmBayesAdaptState(Model, AdaptationData)
% Use an empirical Bayes adaptation to adapt the means of a single state.
% Adaptation algorithm as described by Reynolds et al, "Speaker
% Verification Using Adapted Gaussian Mixture Models", _Digital Signal
% Processing_, V10, Nos. 1-3, pp 19-41, 2000.
%
% Currently only performs means adaptation for a single state
%
% This code is copyrighted 2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

if Model.NumberStates ~= 1 
  error('Only single-state HMMs supported.')
end

RelevanceMu = 16;	% Empirical relevance parameter
Verbosity = 1;

AdaptedModel = Model;
[Components, ObservationCount] = size(AdaptationData);

TerminatingCondition = 0;

% Used for computing the expected value of the observations
% in each state.
Emu = zeros(Model.NumberMixtures, Model.Components);

for sidx = 1:Model.NumberStates
  % Compute state conditional and state/mixture conditional probabilities
  [Pr PrMix] = hmmObsProbDiag(AdaptationData', AdaptedModel, sidx);
  PrMix = squeeze(PrMix);	% Remove singleton dim PrMix(m,1,t)
  
  % Pr(t) contains Pr(x_t | state-s)
  % PrMix(m,t) contains mix-weight_m * Pr(x_t | mix_m, state_s)
  
  % Compute mixture occupancies for current state.
  % Pr(mix_i | x_t)  
  %             mix-weight_i * Pr(x_t | mix_i)
  %  = ----------------------------------------------- 
  %    sum_{j=1}^Mixtures mix-weight_j Pr(x_t | mix_j)
  %
  %  = mix-weight_i * Pr(x_t | mix_i)
  %     ------------------------------
  %	            Pr(x_t)
  
  % Determine expected means for this state.  Although we could set this up
  % as a matrix operation, the large size creates memory allocation havoc.
  % Determine MixOccupancies on a per mixture basis.
  
  for midx = 1:Model.NumberMixtures
    MixOccupancy = (PrMix(midx,:) ./ Pr)';
    MixOccupancyCount = sum(MixOccupancy);
    
    % expected mean given data
    Emu = sum(MixOccupancy(:,ones(1, Model.Components)) ...
	      .* AdaptationData) / MixOccupancyCount;
      
    % determine adaptation weights
    NewWt = MixOccupancyCount / (MixOccupancyCount + RelevanceMu);
    OldWt = 1 - NewWt;
    % adapt current mean to new data
    AdaptedModel.Mix.mu(midx, :, sidx) = ...
	NewWt * Emu + OldWt * Model.Mix.mu(midx, :, sidx);
  end
end
