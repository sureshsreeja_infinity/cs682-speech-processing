% [LogLikelihood, OptPath] = hmmViterbiAux(Model, CachedStateLikelihoods)
% Given an HMM model and a States X Observations matrix of likelihoods,
% determine the log likelihood of the optimal path through the model.
% If OptPath is present, the state sequence associated with the optimal
% path is also returned.
%
% This code is copyrighted 2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 
