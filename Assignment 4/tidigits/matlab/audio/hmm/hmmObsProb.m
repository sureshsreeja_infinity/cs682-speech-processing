function [Probability, MixProbs] = ...
    hmmObsProb(Obs, Model, State, IntMethod, IntArgs)
% [Probability, MixProbs] = ...
% hmmObsProb(Observation, Model, State, IntegrationMethod, IntegrationsArgs)
%
% Computes probability vector of observing a specific multidimensional
% observation in a either a specific state or for all states (if the
% optional state argument is not given or 0).  MixProbs are the
% probabilities for each mixture within each state 
% stored in a Mixture x State matrix.
%
% Note:  We use a mixture of multivariate normal distributions.
%
%	f(x) = 2pi^{-n/2} det(Gamma)^{-1/2} * 
%		exp(-.5 (x-mu)' Gamma^{-1} (x-mu))
%
% The optional integration method specifies what type of integration should
% be used:
%	0 - No integration
%	1 - Test code for 2d observations.  Integration arguments provides
%	    the local variance for each component and the area of an
%	    ellipse within .2535 * std deviation for each direction.
%	    ?? For a standard normal distribution, F(.2535) ~= .6,
%	    so +/- .2535*std will cover .2 of the distribution's spread.
%	    Extending this to two dimensions, we have .2*.2=.04 of the
%	    2 dimensional normal.  IntArgs should be a vector of standard
%	    deviations for the components.
%	    
%	
% This code is copyrighted 1997, 1998, 1999 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(2,5,nargin))

if nargin < 3
  AllStates = 1;
  IntMethod = 0;
else
  AllStates = (State == 0);
  if nargin < 4
    IntMethod = 0;
  end
end;

Mixtures = Model.NumberMixtures;
States = Model.NumberStates;

% Convert column vector observations to row vector
if (size(Obs, 2) == 1)
  Obs = Obs';
end

if AllStates
  
  switch IntMethod
    case 0
      moCOVmo = zeros([Mixtures, States]);
      for s = 1:States;
	for m = 1:Mixtures;
	  MeanOffset = Obs - Model.Mix.mu(m,:,s);
	  moCOVmo(m,s) = MeanOffset * Model.Mix.cov.SigmaInv{m,s} * MeanOffset';
	end
      end
      
      MixProbs = Model.Mix.cov.k .* exp(-.5 * moCOVmo);
      
      % Weight 
      MixProbs = Model.Mix.c .* MixProbs;
      
    case 1
      MixProbs = zeros([Mixtures, States]);      
      moCOVmo = zeros([Mixtures, States]);

      % for .04-.2535 .2535];
      Offsets = [-0.28402269 0.28402269]; % .05
      QtrArea = pi * prod(IntArgs) * Offsets(1) * Offsets(1) / 4;
      for c1 = Offsets * IntArgs(1);
	for c2 = Offsets * IntArgs(2);
	  for s = 1:States
	    for m = 1:Mixtures
	      % Obs + [c1 c2]
	      MeanOffset = Obs + [c1 c2] - Model.Mix.mu(m,:,s);
	      moCOVmo(m,s) = MeanOffset * ...
		  Model.Mix.cov.SigmaInv{m,s} * MeanOffset';
	    end
	  end
	  MixProbs = MixProbs + ...
	      QtrArea * Model.Mix.c .* Model.Mix.cov.k .* exp(-.5 * moCOVmo);
	end
      end
  end
  
  Probability = sum(MixProbs, 1)';

  % Set underflows to minimum probability
  MixProbs(find(MixProbs == 0)) = eps;
  Probability(find(Probability == 0)) = eps;

else
  
  MeanOffset = repmat(Obs, [Mixtures, 1]) - Model.Mix.mu(:,:,State);
  
  moCOVmo = zeros(Mixtures, 1);
  for m = 1:Mixtures;
    MeanOffset = Obs - Model.Mix.mu(m,:,State);
    moCOVmo(m,1) = MeanOffset * Model.Mix.cov.SigmaInv{m,State} * MeanOffset';
  end
  
  MixProbs = Model.Mix.c(:,State) .* Model.Mix.cov.k(:,State) .* ...
      exp(-.5 * moCOVmo);
  
  Probability = sum(MixProbs, 1)';

  % Set underflows to minimum probability
  MixProbs(find(MixProbs == 0)) = eps;
  Probability(find(Probability == 0)) = eps;
  
end
