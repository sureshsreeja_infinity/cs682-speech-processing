function [Distortions, Indices] = ...
    vqklDistortion(CodeBook, Means, Variances, varargin)
% function [Distortions Indices] = 
%	vqklDistortion(Codebook, Means, Variances)
%
% Given a codebook, a set of row vectors representing the Means of an normal
% distribution with independent components, and their respective Variances
% (also row vectors), compute the distortion against each codeword of the
% codebook.
%
% The optional output Indices indicates the indices of the codewords
% which produce minimum distortion with each mean vector.
%
% Optional arguments:
%
%	'Codewords', [Indices] - Only determine the distortion for codeword
%		indices.  When 'Codewords' is specified, the value of Indices
%		is represents the minimal distortion codewords from the
%		within the set of indices.
%
% The Distortions will be returned in a N x Codewords size matrix (unless
% optional argument 'Codeword' is present in which case Distortions will
% be N x 1).
%
% If the optional output argument Indices is present, a vector of minimum
% code word distortion codeword indices is returned as well.
%
% This code is copyrighted 2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

error(nargchk(3,inf,nargin))

n=1;
Codewords = [];
while n <= length(varargin)
  switch varargin{n}
   case 'Codewords'
    Codewords = varargin{n+1}; n=n+2;
   otherwise
    error(sprintf('Bad keyword argument "%s"', varargin{n}));
  end
end

NumVectors = size(Means,1);

if isempty(Codewords) 
  Codewords = 1:CodeBook.N;
else
  if ~ utIsVector(Codewords, 'row')
    Codewords = Codewords';
  end
end

Distortions = zeros(NumVectors, 1);

Extend = ones(CodeBook.N, 1);	% for Tony's trick

for idx=1:NumVectors
  MinDistortion = Inf;
  Mean = Means(idx,:);
  
  MeanOffsetSq = (CodeBook.CodeWords - Mean(Extend,:)) .^ 2;
  
  % KL distance in first direction
  kl1 = (CodeBook.Variances + MeanOffsetSq) ./ CodeBook.Variances;
  
  % Setup matrix of variances for input vector 
  Variance = Variances(idx,:);
  Variance = Variance(Extend, :);
  
  % KL distance in second direction
  kl2 = (Variance + MeanOffsetSq) ./ Variance;
  
  kl = sum((kl1 + kl2 - 2), 2) ./ 2;

  [Distortions(idx), Indices(idx)] = min(kl);

end
%fprintf('stopping in debugger')
%keyboard
