#include <mex.h>
#include <matrix.h>
#include <stdio.h>
#include <math.h>

#include "lib/vqlib.h"

#define COL_INCROW_ELEMENTS(ROWSIZE, COLSIZE)	1
#define COL_INCCOL_ELEMENTS(ROWSIZE, COLSIZE)	(ROWSIZE)

/* Optimization of variance reestimation for independent components */

#define MAXERRMSG	255
#define VEC_COMPONENT_INC	1	/* vectors - next component offset=1 */

/* gateway function
 *
 * LHS Arguments:
 *	Distortion
 *	CodewordIndices
 *	ComponentsProcessed
 * RHS Arguments:
 *	Codebook
 *      MatrixOfRowVectors
 *	HeuristicCodeWordIndex (optional)
 *
 * Given a vector quantizer CodeBook, and a matrix of row vectors,
 * find the minimum distortion encoding using partial distance
 * elimination (PDE).
 *
 * PDE exploits the linear separability of many distortion measures.
 * For each vector to be encode, the distortion for a single code word
 * is computed and used as a candidate minimum distortion codeword.
 * The partial distortions of other codewords are incrementally
 * computed one at a time.  As soon as any partial distortion exceeds
 * the candidate minimum distance, no further computations need to be
 * made.  If the total distortion is less than the candidate
 * distortion, the code word is selected as a new candiate.
 *
 * If the distortion of the first codeword to be selected is low, it
 * will reduce the number of components which need to be examined for
 * subsequent codewords in the encoding process.  For the first vector
 * to be encoded, the user may specify the index of the codeword to be
 * used or the routine will make an uniformed choice.  For subsequent
 * vectors to be encoded, the system uses the minimum distortion
 * codeword of the previous vector.  This is an appropriate choice
 * when there is a fair degree of correlation between vectors, but
 * will always produce the correct result regardless of the correlation.
 *
 * The output Distortion represents the average distortion obtained
 * with the minimum distortion codewords.  The optional ouput
 * arguments CodewordIndices and ComponentsProcessed return the
 * minimum distortion codewords for each vector and the total number
 * of components for which distance was calculated (useful for
 * determining how effective PDE was at pruning the search).
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 * */

void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray 
	    *prhs[])
{
  /* positional parameters */
  const int	CodeBookPos = 0;
  const int	RowVectorsPos = 1;
  const int	HeuristicCodeWordIdxPos = 2;
  const int	InputArgCount = 3;
  
  const int	DistortionPos = 0;
  const int	CodeWordIndexPos = 1;
  const int	ComponentsProcessedPos = 2;
  const int	OutputArgCount = 3;

  CodeBook	CB;
  double const	*VectorPtr, *CodeWordPtr, *Weights;

  mxArray	*MxMinDistortion;

  double	SumDistortions, *MinCodeWordPtr;
  int		MinCodeWordIdx, HeuristicCodeWordIdx;
  int		CWIdx, VecIdx, VectorCount,
		Components, ComponentsProcessed;

  PartialDistortion	DistortionInfo;
    
  /* check args */
  if (nrhs > InputArgCount)
    mexErrMsgTxt("Bad argument count");

  /* Retrieve CodeBook */
  vqCodeBookAccessor(prhs[CodeBookPos], &CB);

  /* Retrieve vectors to encode */
  VectorPtr = mxGetPr(prhs[RowVectorsPos]);
  VectorCount = mxGetM(prhs[RowVectorsPos]);
  Components = mxGetN(prhs[RowVectorsPos]);

  /* check vectors and codewords have the same number of components */
  if (CB.VectorSize != Components) {
    char Message[MAXERRMSG];
    
    sprintf(Message, "Codebook/Vector component mismatch:  "
	    "%d (CodeBook) != %d (Vector)", CB.VectorSize, Components);
    mexErrMsgTxt(Message);
  }
  
  /* Did the user provide a guess as to the minimum code word? */
  if (nrhs > HeuristicCodeWordIdxPos) {
    /* retrieve guess - convert 1:N to 0:(N-1) */
    HeuristicCodeWordIdx =
      (int) mxGetScalar(prhs[HeuristicCodeWordIdxPos]) - 1;
    if (HeuristicCodeWordIdx < 0 || HeuristicCodeWordIdx >= CB.CodeWordCount) {
      char Message[MAXERRMSG];
    
      sprintf(Message, "Heuristic code word index must be between 1:%d",
	      CB.CodeWords);
      mexErrMsgTxt(Message);
    }
  } else {
    HeuristicCodeWordIdx = 0;	/* uninformed guess */
  }

  /* Create array for minimum code word indices if requested */
  if (nlhs > 1) {
    plhs[CodeWordIndexPos] = mxCreateDoubleMatrix(VectorCount, 1, mxREAL);
    MinCodeWordPtr = mxGetPr(plhs[CodeWordIndexPos]);
  } else {
    MinCodeWordPtr = NULL;
  }

  SumDistortions = 0;
  ComponentsProcessed  = 0;
  /* for each vector to encode */
  for (VecIdx = 0; VecIdx < VectorCount; VecIdx++) {

    CodeWordPtr = CB.CodeWords;

    DistortionInfo.MinDistortion =
      vqDistortionTotal(COL_ACCESS(CodeWordPtr, HeuristicCodeWordIdx,
				   0, CB.CodeWordCount),
			COL_NEXTCOL_OFFSET(CB.CodeWordCount, CB.VectorSize),
			VectorPtr,
			COL_NEXTCOL_OFFSET(VectorCount, Components),
			CB.Weights,
			COL_NEXTROW_OFFSET(Components, 1),
			Components);
    MinCodeWordIdx = HeuristicCodeWordIdx;
    ComponentsProcessed += CB.VectorSize;
    
    /* check distortion against each codeword */
    for (CWIdx = 0; CWIdx < CB.CodeWordCount; CWIdx++) {
      /* Don't recompute the Heuristic code word */
      if (CWIdx != HeuristicCodeWordIdx) {
	vqDistortionPartial(CodeWordPtr,
			    COL_NEXTCOL_OFFSET(CB.CodeWordCount, CB.VectorSize),
			    VectorPtr,
			    COL_NEXTCOL_OFFSET(VectorCount, Components),
			    CB.Weights,
			    COL_NEXTROW_OFFSET(Components, 1),
			    Components,
			    &DistortionInfo);
      
	ComponentsProcessed = ComponentsProcessed + DistortionInfo.Components;
	if (DistortionInfo.Distortion < DistortionInfo.MinDistortion) {
	  /* Distortion is smaller than the minimum found so far */
	  DistortionInfo.MinDistortion = DistortionInfo.Distortion;
	  MinCodeWordIdx  = CWIdx;
	}
      }
      /* skip to next codeword */
      COL_INCROW(CodeWordPtr, CB.CodeWordCount, CB.VectorSize);
    }

    SumDistortions += DistortionInfo.MinDistortion;
    if (MinCodeWordPtr) {
      /* Save index smallest dist - Matlab 1:N instead of 0:N-1 */
      *MinCodeWordPtr = (double) MinCodeWordIdx + 1;
      COL_INCROW(MinCodeWordPtr, VectorCount, 1);
    }
    
    /* use correlation to guess next code word */
    HeuristicCodeWordIdx = MinCodeWordIdx;
    
    COL_INCROW(VectorPtr, VectorCount, Components);	/* next vector */
  }
  
  plhs[DistortionPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
  *(mxGetPr(plhs[DistortionPos])) = SumDistortions / VectorCount;

  /* Return the number of components processed if requested
   * (Useful for benchmarking PDE)
   */
  if (nlhs > ComponentsProcessedPos) {
    plhs[ComponentsProcessedPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
    *(mxGetPr(plhs[ComponentsProcessedPos])) = (double) ComponentsProcessed;
  }

}



