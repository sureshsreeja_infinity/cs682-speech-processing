#include <mex.h>
#include <matrix.h>
#include <stdio.h>
#include <math.h>

#include "lib/vqlib.h"

/* Optimization of variance reestimation for independent components */

#define MAXERRMSG	255
#define VEC_COMPONENT_INC	1	/* vectors - next component offset=1 */

/* gateway function
 *
 * LHS Arguments:
 *	Distortion
 *	CodeWordIndex
 *	ComponentsProcessed
 *
 * RHS Arguments:
 *	Codebook
 *      Vector
 *	CodeWordIndex_HeuristicGuess - optional
 *
 * Given a vector quantizer CodeBook, and a single vector Vector, find
 * the minimum distortion encoding using partial distance elimination
 * (PDE).
 *
 * Partial distance elimination exploits the linear separability of 
 * squared error distortion measures.  The distortion for a single
 * code word is computed and used as a candidate minimum distortion
 * codeword.  The partial distortions of other codewords
 * are incrementally computed one at a time.  As soon as any partial
 * distortion  exceeds the candidate minimum distance, no further
 * computations need to be made.   If the total distortion is less
 * than the candidate distortion, the code word is selected as a new
 * candiate.
 *
 * The initial candidate code word may either be done automatically or
 * specified as the optional argument CodeWordIndex_HeuristicGuess.
 * A common heuristic is to use the index of a the last encoded vector
 * in a series.  If there is high correlation between subsequent vectors,
 * this is a good choice.
 *
 * Output is up to three parameters, only the first of which is mandatory:
 * Distortion - the distortion produced by the minimum distortion codeword.
 * CodeWordIndex - The index of the minimum distortion codeword.
 * ComponentsProcessed - An indication of the number of components
 *	that were processed, useful for determining how effective the
 *	PDE was at reducing the computational cost.
 *
 * Note that in order to optimize this heavily used routine, little
 * checking is performed.  Any error checking should be done at higher
 * levels, and this routine should not be invoked by the user without
 * caution.
 *
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 */

void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray 
	    *prhs[])
{
  /* positional parameters */
  const int	CodeBookPos = 0;
  const int	VectorPos = 1;
  const int	HeuristicCodeWordIdxPos = 2;
  const int	InputArgCount = 3;
  
  const int	DistortionPos = 0;
  const int	CodeWordIndexPos = 1;
  const int	ComponentsProcessedPos = 2;
  const int	OutputArgCount = 3;

  CodeBook	CB;
  double const	*Vector, *Weights;
  double	*CodeWordPtr;

  mxArray	*MxMinDistortion;

  double	ComponentsProcessed;
  int		MinCodeWordIdx, HeuristicCodeWordIdx;
  int		Idx;

  PartialDistortion	DistortionInfo;
    
  /* check args */
  if (nrhs > InputArgCount)
    mexErrMsgTxt("Bad argument count");

  /* Retrieve CodeBook */
  vqCodeBookAccessor(prhs[CodeBookPos], &CB);

  /* Retrieve vector to encode */
  Vector = mxGetPr(prhs[VectorPos]);

  /* check vector is same size as codewords, we don't really
   * care if it is row or column vector as long as we have
   * the right number of elements.  For efficiency, we don't
   * verify that this is really a vector; a matrix with the right
   * number of elements will be accepted as well.
   */
  if (CB.VectorSize != (mxGetM(prhs[VectorPos]) * mxGetN(prhs[VectorPos]))) {
    char Message[MAXERRMSG];
    
    sprintf(Message, "Codebook/Vector size mismatch:  "
		      "Codebook: %d x %d, Vector %d x %d",
		      CB.CodeWordCount, CB.VectorSize,
		      mxGetM(prhs[VectorPos]),
		      mxGetN(prhs[VectorPos]));
    mexErrMsgTxt(Message);
  }
  
  /* Did the user provide a guess as to the minimum code word? */
  if (nrhs > HeuristicCodeWordIdxPos) {
    /* retrieve guess - convert 1:N to 0:(N-1) */
    HeuristicCodeWordIdx =
      (int) mxGetScalar(prhs[HeuristicCodeWordIdxPos]) - 1;
  } else {
    HeuristicCodeWordIdx = 0;	/* uninformed guess */
  }

  /* Compute the full distortion of the heuristic code word */
  DistortionInfo.MinDistortion =
    vqDistortionTotal(CB.CodeWords + HeuristicCodeWordIdx,
		      CB.CodeWordCount,
		      Vector, VEC_COMPONENT_INC,
		      CB.Weights, VEC_COMPONENT_INC,
		      CB.VectorSize);
  MinCodeWordIdx = HeuristicCodeWordIdx;
  ComponentsProcessed = CB.VectorSize;

  /* Compute partial distortion of all other codewords */
  for (Idx = 0; Idx < CB.CodeWordCount; Idx++) {
    /* Don't recompute the Heuristic code word */
    if (Idx != HeuristicCodeWordIdx) {
      vqDistortionPartial(CB.CodeWords + Idx, CB.CodeWordCount,
			  Vector, VEC_COMPONENT_INC,
			  CB.Weights, VEC_COMPONENT_INC,
			  CB.VectorSize, 
			  &DistortionInfo);

      ComponentsProcessed = ComponentsProcessed + DistortionInfo.Components;
      if (DistortionInfo.Distortion < DistortionInfo.MinDistortion) {
	/* Distortion is smaller than the minimum found so far */
	DistortionInfo.MinDistortion = DistortionInfo.Distortion;
	MinCodeWordIdx  = Idx;
      }
    }
  }
  
  plhs[DistortionPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
  *(mxGetPr(plhs[DistortionPos])) = DistortionInfo.MinDistortion;

  /* Return minimum distortion code word index if requested */
  if (nlhs > CodeWordIndexPos) {
    plhs[CodeWordIndexPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
    /* Matlab 1:N instead of 0:(N-1) */
    *(mxGetPr(plhs[CodeWordIndexPos])) = (double) MinCodeWordIdx + 1;
  }

  /* Return the number of components processed if requested
   * (Useful for benchmarking PDE)
   */
  if (nlhs > ComponentsProcessedPos) {
    plhs[ComponentsProcessedPos] = mxCreateDoubleMatrix(1, 1, mxREAL);
    *(mxGetPr(plhs[ComponentsProcessedPos])) = (double) ComponentsProcessed;
  }
  
}
