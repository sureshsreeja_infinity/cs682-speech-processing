function [Distortion, CodeWordIdx] = ...
    vqPoggiDistortionSV(VQ, Vector, EstCodeWordIdx)
% [Distortion, Codeword] = 
%	vqPoggiDistortionSV(Codebook, Vector, EstCodeWordIdx)
%
% Fast full-search VQ encoding.
% Given a Poggi VQ codebook and a row vector to be encoded,
% determine the codeword which produces the minimum distortion.  Its
% Distortion and Codeword index are returned.
%
% If a heuristic estimate of the appropriate codeword index is available,
% it may be provided in the optional argument EstCodeWordIx.  Inclusion of
% such information can be used to prune the search significantly and will
% not affect the outcome.  A good heuristic for correlated features
% (i.e. cepstral speech features) is to use the best codeword from the
% previous feature vector.

% Details for Poggi's full-sarch VQ encoder may be found in:
% @Article{poggi93,
%   author = 	 {Poggi, G.},
%   title = 	 {Fast Algorithm For Full-Search VQ Encoding},
%   journal = 	 {Electronics Letters},
%   year = 	 1993,
%   volume =	 29,
%   number =	 12,
%   pages =	 {1141-1142},
%   month =	 {June},
% }
%

error(nargchk(2, 3, nargin));

if nargin < 3
  EstCodeWordIdx = 1;	% no heuristic selection - uninformed decision
end

Weighted = isfield(VQ, 'Weights');	% Are the components weighted?
Components = length(Vector);

% Compute distortion related to EstCodeWordIdx and mean component value
% of vector to be encoded.
if Weighted
  EstDiff = VQ.Weights .* (VQ.CodeWords(EstCodeWordIdx, :) - Vector);
  Xi = mean(VQ.Weights .* Vector);
else
  EstDiff = VQ.CodeWords(EstCodeWordIdx, :) - Vector;
  Xi = mean(Vector);
end
EstMinDist = sum(EstDiff .* EstDiff);

DeltaMinDist = sqrt(EstMinDist / Components);

LowerBound = Xi - DeltaMinDist;
UpperBound = Xi + DeltaMinDist;

Search = find(VQ.Means >= LowerBound & VQ.Means <= UpperBound)';

if 0 
  if length(Search) < length(VQ.Means)
    fprintf('Search: ')
    fprintf(' %d', Search);
  else
    fprintf('No pruning\n');
  end
end

bp = 1;

% silence warnings for now
% kludge in retvals
Distortion = Search(1);
CodeWordIdx = Search(end);
