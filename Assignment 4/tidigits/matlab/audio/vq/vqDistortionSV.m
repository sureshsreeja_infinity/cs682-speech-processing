% [Distortion CodeWordIndex] = vqDistortionSV(CodeBook, Vector)
%
% Given a vector quantizer CodeBook, and a single vector Vector,
% find the minimum distortion encoding.  Distortion represents the
% distance to the vector, and CodeWordIndex is the code word
% which produced the minimum distortion.
%
% Distortion is a Euclidean distortion measure.  If a component
% weighting is present in the CodeBook, it will be used in the
% distortion computation.
%
% This code is copyrighted 1999-2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

