/*
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#include <mex.h>
#include <matrix.h>

#include "vqlib.h"

/* vqCodeBookAccessor
 * Expects pointers to a populated Matlab VQ codebook and an empty C
 * codebook.  Populates the C codebook from the Matlab codebook.
 */
void vqCodeBookAccessor(mxArray const *MxSourceBook, CodeBook *DestBook)
{
  mxArray const		*MxCodeWords, *MxWeighted;
  CodeBook		Book;
  int			Weighted;

  MxCodeWords = mxGetField(MxSourceBook, 0, "CodeWords");
  if (! MxCodeWords) {
    mexErrMsgTxt("Unable to access field CodeWords.  Valid codebook?");
  }
  DestBook->CodeWords = mxGetPr(MxCodeWords);
  DestBook->CodeWordCount =  mxGetM(MxCodeWords);
  DestBook->VectorSize = mxGetN(MxCodeWords);

  /* Weighted distance?  If so, extract weights */
  MxWeighted = mxGetField(MxSourceBook, 0, "Weighted");
  Weighted = MxWeighted ? (int) mxGetScalar(MxWeighted) : 0;
  if (Weighted) {
    DestBook->Weights = mxGetPr(mxGetField(MxSourceBook, 0, "Weights"));
  } else {
    DestBook->Weights = NULL;
  }

  return;
}
