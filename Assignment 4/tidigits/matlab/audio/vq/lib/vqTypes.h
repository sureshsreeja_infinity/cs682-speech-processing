/*
 * This code is copyrighted 2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

#ifndef VQTYPES_H
#define VQTYPES_H 1

/* CodeBook data type */

typedef struct {
  /* Matrix of codewords.  Each row is a codeword.  Matrix is expected
   * in column major order.
   */
  double	*CodeWords;
  double	*Weights;	/* NULL if no weighting */
  int		VectorSize;	/* dimensionality */
  int		CodeWordCount;
} CodeBook;

/* structure used for communicating information about partial
 * distance elimination distortions.
 */

typedef struct {
  double	MinDistortion;	/* Stop computation if Distortion exceeds */
  double	Distortion;	/* partially computed value */
  int		Components;	/* Number of components checked */
  int		Min;
} PartialDistortion;

#endif
