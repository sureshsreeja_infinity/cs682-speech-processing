/*
 * This code is copyrighted 1999-2001 by Marie Roch.
 * e-mail:  marie.roch@ieee.org
 *
 * Permission is granted to use this code for non-commercial research
 * purposes.  Use of this code, or programs derived from this code for
 * commercial purposes without the consent of the author is strictly
 * prohibited. 
 *
 */

double vqDistortionTotal(const double *CodeWordPtr, int NextCodeWordComponent,
			 const double *VectorPtr, int NextVectorComponent,
			 const double *WeightsPtr, int NextWeightsComponent,
			 int Components)
{
  int		Component;
  double	Diff, Distortion;

  Distortion = 0;
  
  if (WeightsPtr) {
    for (Component = 0; Component < Components; Component++) {
      Diff = (*CodeWordPtr - *VectorPtr);
      Distortion += *WeightsPtr * Diff * Diff;
      CodeWordPtr += NextCodeWordComponent;	/* step through matrix */
      VectorPtr += NextVectorComponent;
      WeightsPtr += NextWeightsComponent;
    }
  } else {
    for (Component = 0; Component < Components; Component++) {
      Diff = *CodeWordPtr - *VectorPtr;
      Distortion += Diff * Diff;
      CodeWordPtr += NextCodeWordComponent;	/* step through matrix */
      VectorPtr += NextVectorComponent;
    }
  }

  return Distortion;
}
