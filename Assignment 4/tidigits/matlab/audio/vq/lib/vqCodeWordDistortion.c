/* vqCodeWordDistortion - Compute the total distortion between a specific
 * code word and vector.
 *
 * Arguments:
 *	CodeWordPtr - points to a specific code word in the code book
 *	NextComponent - Offset to move to the next component of the code
 *		code word.
 *	VectorPtr - Vector against which distortion is to be checked.
 *	VectorSize - Number of components
 *	WeightsPtr - Pointer to an array of component weights or NULL
 *		if no component weighting is to be used.
 */
double vqCodeWordDistortion(double *CodeWordPtr, int NextComponent,
			    double *VectorPtr, int VectorSize,
			    double *WeightsPtr)
{
  int		Component;
  double	Diff, Distortion;

  Distortion = 0;
  
  if (WeightsPtr) {
    for (Component = 0; Component < VectorSize; Component++) {
      Diff = (*CodeWordPtr - *VectorPtr);
      Distortion += *WeightsPtr * Diff * Diff;
      CodeWordPtr += NextComponent;	/* step through matrix */
      VectorPtr++;
      WeightsPtr++;
    }
  } else {
    for (Component = 0; Component < VectorSize; Component++) {
      Diff = *CodeWordPtr - *VectorPtr;
      Distortion += Diff * Diff;
      CodeWordPtr += NextComponent;	/* step through matrix */
      VectorPtr++;
    }
  }

  return Distortion;
}
