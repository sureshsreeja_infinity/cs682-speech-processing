function vqPlotBook(Book, varargin)
% vqPlotBook(CodeBook, PlotArguments)
% Plots the the first three dimensions of a codebook.
% Perhaps later we'll get creative and do a principal components
% analysis...
%
% This code is copyrighted 1997-1998 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

newplot

dim = size(Book, 2);
switch dim
    case 1
        error('Cannot plot 1d codebook');
    case 2
        plotfn = @plot;
        codewords = {Book(:,1), Book(:,2)};
    otherwise
        plotfn = @plot3;  % Plot first 3
        codewords = {Book(:,1), Book(:,2), Book(:,3)};
end

if length(varargin)
  plotfn(codewords{:}, varargin{:})
else
  plot3(codewords{:}, 'b.')
end
