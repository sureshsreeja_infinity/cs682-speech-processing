function [Codebook, Indices] = vqCreateModel(N, TrainingData, varargin)
% [Codebook Indices] = vqCreateModel(N, TrainingData, OptArgs)
% Construct an N word codebook from the row vector training data.
% If the output argument Indices is provided, the code word indices
% to which each of the training data are quantized will also be
% returned.
%
% Optional arguments:
%
%	'Distortion', D - Distortion measure for VQ analysis:
%		'euclidean'- Euclidean (default)
%		'std' - The standard deviation is computed for each
%			component and used to weight the distortion measure.
%		'mahalanobis' - Mahalanobis distance.  We assume that
%			components are independent, making this an
%			inverse variance weighting.
%		Mutually exclusive with 'Weights'
%	'Weights', WeightVector - Permits an arbitrary weighting
%		scheme for the distortion measure.  
%		Mutually exclusive with 'Distortion'
%	'K-means', K - K Means algorithm used for code book generation
%               'user' - Use the given TrainingData matrix as the mean vectors.
%                       This allows the creation of vector quantizers
%                       where clustering is done with an arbitrary method.
%		'lbg' - Linde, Buzo, Gray
%		'ska' - Stochastic K-Means algorithm
%		'bhattacharyya' - K-Means algorithm where the distortion
%			is based upon the Bhattacharyya bound.
%			The Bhattacharyya bound is an upper bound on
%			Pr(error|x) when x is classified by two normal
%			distributions.  As distributions which are
%			similar will have smaller Bhattacharyya distances,
%			similar clusters will be merged.  This implies
%			that each point of the TrainingData must have
%			an associated normal distribution.  
%
%			We assume that the training data represents the
%			means of a normal distribution with independent
%			components.  These variances are passed in as
%			row vectors with the argument
%				'Variances', VarianceRowVectors 
%			which becomes mandatory when Bhattacharrya
%			construction is selected.  
%			
%			After construction, distortion will be evaluated
%			based upon the Mahalanobis distance.  If the,
%			'Distortion' keyword is specified, it is ignored.
%			BHATTACHARYYA IS CURRENTLY BROKEN
%		'kl' - symmetric Kullback-Leibler divergence
%	'Verbosity, N - Verbosity level
%		0 - Quiet
%		1 - Summary
%		2+ - algorithm dependent higher levels of feedback
%
% This code is copyrighted 2001-2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 


% defaults
Verbosity = 0;
CodeBook.DistortionMetric = 'euclidean';
CodeBook.KMeans = 'lbg';

% Some of the arguments will be passed through to the various
% k-means algorithms.  Delete any that should not be passed.
n=1;
while n <= length(varargin)
  switch varargin{n}
   case 'Verbosity'
    Verbosity = varargin{n+1}; n=n+2;

   case 'K-means'
    % Clustering method
    CodeBook.KMeans = varargin{n+1};
    varargin(n:n+1) = [];	% don't pass through
    
   case 'Distortion'
    CodeBook.DistortionMetric = varargin{n+1};
    varargin(n:n+1) = [];	% don't pass through
    
   case 'Weights'
    CodeBook.DistortionMetric = 'creator-supplied-weights';
    UserWeights = varargin{n+1};
    if ~ isa(UserWeights, 'double')
      error('Weights must be a vector of double precision floats');
    end
    if length(UserWeights) ~= size(TrainingData, 2)
      error('Weights must be of same dimension as training data');
    end
    varargin(n:n+1) = [];	% don't pass through
    
   otherwise
    n=n+2;	% Assume keyword/value pairs
  end
end

switch CodeBook.DistortionMetric 
  % Note on non-Euclidean distance measures:
  % The VQ routines weight each component before computing the
  % distance.  Consequently, if you wish to weight the distance
  % as opposed to the component, you must specify its sqrt as the 
  % weight.
 case 'creator-supplied-weights'
  CodeBook.Weighted = 1;
  CodeBook.Weights = UserWeights;
 case 'euclidean'
  % Euclidean distance measure
  CodeBook.Weighted = 0;
 case 'std'
  % Weight by 1 / standard deviation
  Sigma = var(TrainingData);
  CodeBook.Weighted = 1;
  CodeBook.Weights = 1 ./ sqrt(Sigma);
 case 'mahalanobis'
  % Weight by 1 / variance
  Sigma = var(TrainingData);
  CodeBook.Weighted = 1;
  CodeBook.Weights = 1 ./ Sigma;
 otherwise
  error(sprintf('Bad Model.Info.DistortionMetric value:  %s', ...
		Model.Info.DistortionMetric));
end

switch CodeBook.KMeans
 case 'user'
  % passed in the means themselves
  Codebook.KMeans = 'creator-supplied-codewords';
  Codebook.CodeWords = TrainingData;
  Codebook.N = size(Codebook.CodeWords, 1);
  Indices = 1:Codebook.N;         % Codewords/TrainingData matches
  
 case 'lbg'
  % Linde Buzo Gray codebook generation with codeword splitting
  [Codebook Indices] = vqNWordLBG(CodeBook, N, TrainingData, varargin{:});
 case 'ska'
  % Stochastic K-Means algorithm
  [Codebook, Indices] = vqSKA(CodeBook, N, TrainingData, varargin{:});
 case 'bhattacharyya'
  % Use the Bhattacharrya distance.
  [Codebook, Indices] = ...
      vqBhattacharyya(CodeBook, N, TrainingData, varargin{:});
 case 'kl'
  [Codebook, Indices] = vqkl(CodeBook, N, TrainingData, varargin{:});
 otherwise
  error(sprintf('Bad K-Means specification "%s"', CodeBook.KMeans));
end
