function Vectors = vqCreateTestData(Points, Clusters, StdDev, ...
    Dimension)
% RowVectors = vqCreateTestData(Points, Clusters, StdDev, Dimension)
% Creates a set of random vectors from C clusters.  P points are
% randomly partitioned into the clusters and normally distributed
% about each cluster with a standard deviation specified by StdDev.
% The cluster means are randomly chosen from a uniform distribution
% and scaled by a function of the StdDev.
%
% This code is copyrighted 1997, 1998 by Marie Roch.
% e-mail:  marie-roch@uiowa.edu
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

Spread=5*StdDev;	% Spread around each distribution
Means = rand(Clusters,Dimension)*Spread;	% Pick a random set of means

% Pick at random to which distribution (1:Mixtures) each of the
% N ranom points will belong.
Indices = floor(rand(Points,1) * Clusters)+1; 

% compute data.  Normal spread about the each mean. 
Vectors = randn(Points, Dimension)*StdDev + Means(Indices,:);

