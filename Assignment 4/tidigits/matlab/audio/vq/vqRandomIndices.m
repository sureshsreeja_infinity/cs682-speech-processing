function Indices = vqRandomIndices(N, Max)
% Indices = vqRandomIndices(N, Max)
% Generate a row vector of N random indices between 1 and Max
%
% This code is copyrighted 2003 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

RandomIndices = randperm(Max);
Indices = RandomIndices(1:N);
