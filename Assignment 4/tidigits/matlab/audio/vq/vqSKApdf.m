function Probs = vqSKApdf(Beta, Distortions)
% function Probs = vqSKApdf(Beta, Distortions)
% Beta - Similar to system temperature for simulated annealing,
%	although higher values indicate less volatility.
% Distortions(TrainingSet X Codewords) - distortion between each
%	TrainingSet vector and codeword.
%
% Given the current system temperature, and a full set of distortions for
% the training vectors, determine a set of discrete probability
% distributions according to the Kovesi et al's stochastic K-means
% algorithm.  (See vqSKA for a complete reference.)
%
% This code is copyrighted 2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

DistortionsT = Distortions';
[CodeWordN, TrainN] = size(DistortionsT);
uDist = mean(DistortionsT);
Numerators = exp(-Beta * DistortionsT ./ uDist(ones(CodeWordN, 1), :));
Denominators = sum(Numerators);

% Check for undeflow
UnderflowCols = find(~ Denominators);

if ~ isempty(UnderflowCols)
  % Denominator will only be near zero for very high Beta which
  % implies a stable system.  Consequently, place all of the 
  % probability mass in the closest codeword.
  [MinDist, MinCodeWordIndices] = min(DistortionsT);
  Probs = zeros(CodeWordN, TrainN);
  Probs((UnderflowCols - 1)*CodeWordN + MinCodeWordIndices(UnderflowCols)) = 1;
  
  % Set up non underflowing columns normally
  ValidCols = find(Denominators);
  Probs(:,ValidCols) = Numerators(:, ValidCols) ./ ...
      Denominators(ones(CodeWordN, 1), ValidCols);
  
  warning([ ...
      sprintf('vqSKApdf:  Underflow occurred Beta %f vectors [', Beta) ...
      sprintf('%d ', UnderflowCols), ...
      '] all prob mass placed in closest codeword']);
else
  % No underflow - easy!
  Probs = Numerators ./ Denominators(ones(CodeWordN, 1), :);
end
