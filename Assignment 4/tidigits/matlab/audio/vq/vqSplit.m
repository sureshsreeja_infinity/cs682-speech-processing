function CodeBook = vqSplit(CodeBook, Data, Indices, SplitWords)
% CodeBook = vqSplit(CodeBook, Data, Indices, SplitWords)
% Perform a binary split of a codebook.
% Each selected codeword is split into two parts, each perturbed by
% +/- epsilon where epsilon is set to a small percentage of the
% standard deviation.  
%
% Data is a the training data and is used to determine the offset.
% Indices indicates to which codeword each datum belongs.
%
% If the vector SplitWords is supplied, only those codewords
% present in the vector will be split.  Otherwise, all
% codewords are split.
%
% This code is copyrighted 1997-2005 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

[Last, Dim] = size(CodeBook.CodeWords);

if nargin < 4
  SplitWords = 1:Last;
end

SplitWordsN = length(SplitWords);
SplitWordsNew = Last + (1:SplitWordsN);

MinVector = 1e-20;
MinVector = MinVector(ones(1, Dim));  % create epsilon vector
MinVectorNorm = norm(MinVector);

% Determine pertubration vectors
SplitWordsPerturb = zeros(SplitWordsN, Dim);
for k=SplitWords
  % Find vectors in partition
  PartitionIndices = find(Indices == k);
  if length(PartitionIndices) < 1
    warning('Codeword %d has %d exemplars - splitting anyway\n', k, length(PartitionIndices))
    SplitWordsPerturb(k,:) = MinVector;
  else
    % ad hoc, pick epsilon in largest directions of data set
    EpsVector = 1e-10 * std(Data(PartitionIndices, :));
    EpsNorm = norm(EpsVector);
    % If too small, use the default epsilon
    if EpsNorm < MinVectorNorm
      SplitWordsPerturb(k,:) = MinVectorNorm;
    else
      SplitWordsPerturb(k,:) = EpsVector;
    end
  end
end

% Copy code words to be split onto end of codebook.
CodeBook.CodeWords(end+1:end+SplitWordsN,:) = ...
    CodeBook.CodeWords(SplitWords, :);
CodeBook.N = size(CodeBook.CodeWords, 1);

% Perturb vectors
for CodeWord = 1:SplitWordsN
  CodeBook.CodeWords(SplitWords(CodeWord),:) = ...
      CodeBook.CodeWords(SplitWords(CodeWord),:) + ...
      SplitWordsPerturb(SplitWords(CodeWord),:);
  
  CodeBook.CodeWords(SplitWordsNew(CodeWord),:) = ...
      CodeBook.CodeWords(SplitWordsNew(CodeWord),:) - ...
      SplitWordsPerturb(SplitWords(CodeWord),:);
end
