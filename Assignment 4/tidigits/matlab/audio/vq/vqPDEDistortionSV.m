% [Distortion CodeWordIndex ComponentsProcessed] = ...
%	vqPDEDistortionSV(CodeBook, Vector, CandidateCodeWordIndex)
%
% Given a vector quantizer CodeBook, and a single vector Vector, find the
% minimum distortion encoding using partial distance elimination.
% Distortion represents the distance to the vector, and
% CandidateCodeWordIndex is the code word which produced the minimum
% distortion.
%
% If the CandidateCodeWordIndex is provided, it will be used to determine
% the initial lower bound on distortion.  Its presence is not mandatory,
% but proper selection can yield large performance improvements.  When
% there is high correlation between vectors, the index of the previous
% vector serves as an excellent heuristic.
%
% Distortion is a Euclidean distortion measure.  If a component
% weighting is present in the CodeBook, it will be used in the
% distortion computation.
%
% Distortion is 
%
% This code is copyrighted 2001 by Marie Roch.
% e-mail:  marie.roch@ieee.org
%
% Permission is granted to use this code for non-commercial research
% purposes.  Use of this code, or programs derived from this code for
% commercial purposes without the consent of the author is strictly
% prohibited. 

