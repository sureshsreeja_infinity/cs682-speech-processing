function u = VQ_MeanMinEucDist(ColVecs, Codebook)
% VQ_MeanMinEucDist(ColVecs, Codebook)
%
% Compute the average minimum distortion of all column vectors ColVecs
% with respect to the given Codebook (column vector codewords).

% Compute pair wise distortions
% distortions(i,j) is distortion between ColVecs(:,i) and Codebook(:,j)
distortions = distortion3(ColVecs, Codebook);

% For each ColVec i, min distortion codeword is one with smallest
% distortion.  Take average of smallest distortions
u = mean(min(distortions));
