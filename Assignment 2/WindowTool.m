function varargout = WindowTool(varargin)
% WINDOWTOOL MATLAB code for WindowTool.fig
%      WINDOWTOOL, by itself, creates a new WINDOWTOOL or raises the existing
%      singleton*.
%
%      H = WINDOWTOOL returns the handle to a new WINDOWTOOL or the handle to
%      the existing singleton*.
%
%      WINDOWTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WINDOWTOOL.M with the given input arguments.
%
%      WINDOWTOOL('Property','Value',...) creates a new WINDOWTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before WindowTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to WindowTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
%
% NOTES on GUI callbacks in Matlab:
% All callbacks have three arguments:
%   hObject - A handle to the current object being called,
%       usually whatever the user clicked on (e.g. popup menu)
%   eventData - reserved for future versions of Matlab
%   handles - A structure array of handles to the components of
%       the figure, menus, etc.  In this GUI, the relevant handles 
%       are:
%           FreqDomain - axes for frequency domain plots
%           TimeDomain - axes for time domain plots
%           window - The popup menu for window selection
%   In adddition, there is a UserData field that is reserved for
%   the user.  In this GUI, we store the fields x and Fs for the
%   signal and sample rate to be plotted.
%
%   Note that there is no need for you to modify any of the data
%   structures contained in handles.  However, if you did, you 
%   would need to save any changes with the guidata function
%   before exiting the callback.



% Edit the above text to modify the response to help WindowTool

% Last Modified by GUIDE v2.5 01-Oct-2012 13:45:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @WindowTool_OpeningFcn, ...
                   'gui_OutputFcn',  @WindowTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before WindowTool is made visible.
function WindowTool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to WindowTool (see VARARGIN)

% Choose default command line output for WindowTool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes WindowTool wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = WindowTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% Auxilary function to plot time and frequency domain
% This is not called directly by the GUI but rather from
% the callback functions.
function plot_dft(handles)
% plot_dft(handles)
% Plot the discrete Fourier transform of the signal
% windowed with the current window function.
% handles - Structure with figure handles and user data containing
%           the signal (handles.UserData.x and handles.UserData.Fs)
%           to plot.

x = handles.UserData.x;  % easier to read...
Fs = handles.UserData.Fs;



% To do:
% For each selected window:
%  Plot the spectrum of the signal 
%    on axis handles.FreqDomain.  Units
%   should be Hz vs dB.  Only plot up to the Nyquist rate.
% Plot original signal and windowed signal on handles.TimeDomain
%   (distinguish the two signals and use the legend command to label)

% Hints:  
% Window functions in Matlab return a window of a desired size,
% e.g. hamming(160) returns a 160 sample Hamming window
% Window functions:  hamming, blackmanharris, rectwin
% Don't forgot to use point by point multiplication between the
% signal and the window.  Your window should be the same size
% as the signal (use the length or size functions to determine this).
%
% You will need to create time and frequency axes.
% The time axis is determined by the number of signal samples and the
% sample rate.
%
%
% Since you are working with multiple axes that have already
% been defined, don't forget to pass the axes handle as the first
% argument when setting labels etc.

% =========== provide your code here ================
% You only need print the code from this subroutine,
% but the electronic copy should have the complete code.

% Build time and frequency axes
% ...

numSamples = length(x); %number of samples in the data
timeAxes = (0:numSamples-1)/Fs;
freqAxes = 0:1/numSamples* Fs:Fs/2 - 1/numSamples;

%converting input signal to frequency domain using FFT
X = fft(x);

%--------------------Hamming Window---------------------------------------%
h = hamming(numSamples); %generating Hamming window of the same size as input.
H = fft(h); %hamming window in frequency domain
xconvh = conv(H,X); %Convolution of input signal and hamming window in frequency domain
xconvhMagSq = xconvh .* conj(xconvh);  %square of magnitude
xconvhDb = 10*log10(xconvhMagSq); %conversion to Db.
xconvhDb = xconvhDb(1:ceil((numSamples+1)/2)); %Taking samples only till nyquist rate

%--------------------Rectangular Window-----------------------------------%
r = rectwin(numSamples); %generating Rectangular window of the same size as input.
R = fft(r); %rectangular window in frequency domain
xconvr = conv(R,X); %Convolution of input signal and rectangular window in frequency domain
xconvrMagSq = xconvr .* conj(xconvr); %square of magnitude
xconvrDb = 10*log10(xconvrMagSq); %conversion to Db.
xconvrDb = xconvrDb(1:ceil((numSamples+1)/2)); %Taking samples only till nyquist rate

%--------------------Blackman-Harris Window-------------------------------%
bh = blackmanharris(numSamples); %generating blackman-harris window of the same size as input.
BH = fft(bh); %blackman-harris window in frequency domain
xconvbh = conv(BH,X); %Convolution of input signal and blackman-harris window in frequency domain
xconvbhMagSq = xconvbh .* conj(xconvbh); %square of magnitude
xconvbhDb = 10*log10(xconvbhMagSq); %conversion to Db.
xconvbhDb = xconvbhDb(1:ceil((numSamples+1)/2)); %Taking samples only till nyquist rate

% Put popup menu handles in row vector so we can
% loop on them
windows = [handles.window, handles.window2];

colors = {'b', 'm'};  % plot colors for windows, e.g. plot(t, x, colors{1});

for widx = 1:length(windows)
    
    % Retrieve the name of the window function
    windowIdx = get(windows(widx), 'Value');   % Index of currently selected
    windowStr = get(windows(widx), 'String'); % Cell array of windows
    % selected name: rectangular, Hamming, Blackman-harris
    windowname = windowStr{windowIdx};    

    % Save the window name so that we can add a legend indicating
    % what window functions are used.  Note that the curly braces { }
    % indicate a cell array that can hold any type of data (poymorphic
    % array).
    legends{widx} = windowname; 
    
    % switch on the windowname and determine the window function
    % to apply and apply it.  Note that the windowname might
    % be 'No Display' in which you should skip the current loop
    % iteration with a continue statement.
    % ...
    
    %define labels for all the axes
    xlabel(handles.TimeDomain, 'Time (s)');
    ylabel(handles.TimeDomain, 'Amplitude (counts)');
    
    xlabel(handles.FreqDomain, 'Frequency (Hz)');
    ylabel(handles.FreqDomain, 'dB rel.');
    
    %differentiate between windows using colors
    if widx==1
        c= colors{1};
    else
        c= colors{2};
    end
    
    %switch between different combinations of windows on data
    switch windowname
        case 'rectangular'
            plot(handles.TimeDomain, timeAxes,x.*rectwin(length(x)),c); %time domain multiplication with rectangular window
            hold(handles.TimeDomain, 'on');
            plot(handles.FreqDomain, freqAxes, xconvrDb,c); %freq domain convolution with rectangular window
            hold(handles.FreqDomain, 'on');
        case 'Hamming'
            plot(handles.TimeDomain, timeAxes,x.*hamming(length(x)),c); %time domain multiplication with hamming window
            hold(handles.TimeDomain, 'on');
            plot(handles.FreqDomain, freqAxes, xconvhDb,c); %freq domain convolution with hamming window
            hold(handles.FreqDomain, 'on');
        case 'Blackman-harris'
            plot(handles.TimeDomain, timeAxes,x.*blackmanharris(length(x)),c); %time domain multiplication with blackman-harris window
            hold(handles.TimeDomain, 'on');
            plot(handles.FreqDomain, freqAxes, xconvbhDb,c); %freq domain convolution with blackman-harris window
            hold(handles.FreqDomain, 'on');
        case 'No Display'
            continue;
    end
    
        
    % Plot time domain, use axis handles.TimeDomain
    % Don't forget to use hold on for this axis so that subsequent
    % iterations don't make previous ones disappear.
    % ...
    
    % Compute and plot frequency domain, use handles.FreqDomain
    % ...
    %plot(handles.FreqDomain,normfreq,ydb);
    
end

% Release the holds so that plots are replaced on the next call
hold(handles.TimeDomain, 'off');
hold(handles.FreqDomain, 'off');
switch legends{2}
    case 'No Display'
        % If the second legend is no display, justuse the first one.
        for h = [handles.FreqDomain, handles.TimeDomain]
            legend(h, legends{1});
        end
    otherwise
        for h = [handles.FreqDomain, handles.TimeDomain]
            legend(h, legends{:});
        end        
end


% --- Executes on selection change in window.
function window_Callback(hObject, eventdata, handles)
% hObject    handle to current object, in this case the window popup
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% User may have changed the window type, replot the DFT
plot_dft(handles);

% --- Executes during object creation, after setting all properties.
function window_CreateFcn(hObject, eventdata, handles)
% hObject    handle to window (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in newsignal.
function newsignal_Callback(hObject, eventdata, handles)
% hObject    handle to newsignal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% dialog to request file name and directory
[fname, fdir] = uigetfile('*.wav', 'Open wavefile');

if ischar(fname)
    % User made selection
    [x, fs] = wavread(fullfile(fdir, fname));  % Read it in
    handles.UserData.x = x;
    handles.UserData.Fs = fs;
    guidata(hObject, handles);  % Save the data
    plot_dft(handles);  % Update the DFT plot. 
end


% --- Executes during object creation, after setting all properties.
function window2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to window2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
