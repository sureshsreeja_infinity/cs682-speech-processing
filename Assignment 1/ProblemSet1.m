%Speech Processing
%Problem Set1
%By: Pratik Sharma, pratik9891@gmail.com
%RedID: 817394741

%Read the audio file
[x,Fs] = wavread('talking.wav');

if size(x,2) > 1
    error('Only single channel files allowed');
end

samples_N = size(x,1);

%time-domain analysis
t = (0:samples_N-1)/Fs; %time for the audio input
figure('Name','Time domain waveform');
annotation('textbox', [0.15, 0.93, 0.4, 0.05], 'string', 'Discretized data after sampling at 16000Hz')
plot(t,x);
xlabel('Time (s)');
ylabel('Amplitude');

%--------------------------------------------------------------------------

framelength_ms = 10;
framelength_s = 10/1000; % conversion to seconds
framelength_N = floor(framelength_s *Fs); % length of one frame


%calculate number of frames in the entire audio
frames_N = floor(samples_N/framelength_N);
if rem(samples_N,framelength_N) > 0
    frames_N = frames_N - 1;
end


intensity_db = zeros(frames_N, 1);
for idx = 1:frames_N
    start = (idx-1)*framelength_N+1;
    stop = idx*framelength_N - 1;
    frame_samples = x(start:stop);
    
    power = sum(frame_samples .^2)/length(frame_samples);
    intensity_db(idx) = 10*log10(power);
end


figure('Name','Intensity Vs. time plot');
t2 = (0:frames_N -1) *framelength_s;
plot(t2,intensity_db);
xlabel('Time(s)');
ylabel('Intensity (db)');
annotation('textbox', [0.15, 0.93, 0.72, 0.05], 'string', 'Calculating average intensity over a 10 milli sec frame from the time doamin samples');
%--------------------------------------------------------------------------


figure('Name', 'Intensity histogram');
hist(intensity_db, 50);
intensityax = gca;
annotation('textbox', [0.15, 0.93, 0.35, 0.05], 'string', 'Histogram analysis of Intensity data')
xlabel('Intenstiy (db)');
ylabel('Counts');
%--------------------------------------------------------------------------

%noise estimate
silencestop_s = 1.3;
silencestop_N = floor(silencestop_s/framelength_s);
silence_train = intensity_db(1:silencestop_N);
figure('Name', 'Silence intensity estimate histogram')
hist(silence_train, 50);
annotation('textbox', [0.15, 0.93, 0.6, 0.05], 'string', 'Analysis of data from 0 sec - 1.3 sec (Assumed to be silence)')
silenceax = gca;  % Save axis of silence distribution
xlabel('Intensity (db)');
ylabel('Counts');

% Make the noise intensity histogram have the same scale as the
% noise and speech intensity histogram
xrange = get(intensityax, 'XLim');
yrange = get(intensityax, 'YLim');
set(silenceax, 'XLim', xrange);
set(silenceax, 'YLim', yrange);
    
mu_noise = mean(silence_train);
var_noise = var(silence_train);
fprintf('Silence estimate:  mean %f variance %f\n', mu_noise, var_noise);
%--------------------------------------------------------------------------

% Speech intensity histogram 
speechstart_s = 1.3;
speechstart_N = floor(speechstart_s/ framelength_s);
speechstop_s = 2.8;
speechstop_N = floor(speechstop_s/framelength_s);
speech_train = intensity_db(speechstart_N:speechstop_N);
figure('Name','Speech intensity estimate histogram');
annotation('textbox', [0.15, 0.93, 0.6, 0.05], 'string', 'Analysis of data from 1.3 sec to 2.8 sec (Assumed to be speech)')
hist(speech_train,50);
xlabel('Intensity (db)');
ylabel('Counts');
speechax = gca;
xrange = get(intensityax, 'XLim');
yrange = get(intensityax, 'YLim');
set(speechax, 'XLim', xrange);
set(speechax, 'YLim', yrange);


mu_speech = mean(speech_train);
var_speech = var(speech_train);
fprintf('Speech estimate: mean %f and variance %f\n',mu_speech,var_speech);
%--------------------------------------------------------------------------

% A Normal "bell curve" probability density function (pdf)
% is defined by its mean and variance.
% Let's see how well it fits our noise histogram
[noiselikelihoodax,X1,fx1] = add_normplot(silenceax, xrange, mu_noise, var_noise);
[speechlikelyhood,X2,fx2] = add_normplot(speechax, xrange, mu_speech, var_speech);
%--------------------------------------------------------------------------

%intersection point
[I1,I2] = normplot_eq_solver(mu_noise,var_noise,mu_speech,var_speech);

threshold_x = max(I1,I2);
threshold_y = normpdf(threshold_x, mu_speech, sqrt(var_speech));
%--------------------------------------------------------------------------


% speech and noise normaplots combined
figure('Name', 'Intensity histogram');
hist(intensity_db, 50);
xlabel('Time(s)');
ylabel('Intensity (db)');
totalax = gca;
xrange = get(intensityax, 'XLim');
yrange = get(intensityax, 'YLim');
set(totalax, 'XLim', xrange);
set(totalax, 'YLim', yrange);
parent = get(totalax, 'Parent');
combinedpdfax = axes('Position', get(totalax, 'Position'),  ... % put over
    'Color', 'none', ... % don't fill it in
    'YAxisLocation', 'right', ...  % Y label/axis location
    'XTick', [], ...  % No labels on x axis
    'YColor', 'b', ... % blue
    'Parent', parent); 

hold (combinedpdfax, 'on');
annotation('textbox', [0.15, 0.93, 0.55, 0.05], 'string', 'Threshold calculated by equating equations of each pdf')
scatter(threshold_x,threshold_y);
plot([threshold_x threshold_x],[threshold_y 0],'r','LineWidth',2,'LineStyle','-');
text(threshold_x+1,threshold_y,'Threshold','FontSize', 12);
plot(combinedpdfax, X1, fx1,'b',X2,fx2,'g');
%--------------------------------------------------------------------------

%Time domain plot of speeech and noise samples based on threshold
figure('Name','Result');
plot(t2,intensity_db,'Color', [0 0 0], 'LineWidth', 0.001);
hold on;
t2 = (0:frames_N -1) *framelength_s;
for idx = 1:frames_N
    if intensity_db(idx) > threshold_x;
        %scatter(t2(idx),intensity_db(idx),20, [0 0 1], 'd');  
        %scatter takes up a lot of time, therefore using plot!
        sound = plot(t2(idx),intensity_db(idx),'d','Color', [0 0 1]);
    else
        %scatter(t2(idx),intensity_db(idx),5, [1 0 0], 'o');
        noise = plot(t2(idx),intensity_db(idx),'o','MarkerSize',3,'Color', [1 0 0]);
    end
end
legend([sound noise],'Speech', 'Noise', 'Location', 'NorthEast');
annotation('textbox', [0.15, 0.93, 0.78, 0.05], 'string', 'Data labeled as speech/noise based on the threshold calculated using pdf of training data')
xlabel('Time(s)');
ylabel('Intensity (db)');
hold off;
