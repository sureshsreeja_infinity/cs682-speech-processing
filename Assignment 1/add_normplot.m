function [pdfax,X,fx] = add_normplot(PeerAx, xrange, mu, sigma2)
% pdfax = add_normplot(PeerAx, xrange, mu, sigma2)
% Given an axis, an xrange [min, max] over which to plot
% it, add a second axis and plot a normal distribution over
% the xrange.
% mu and sigma2 are the means and variances which must be either
% scalars or row vectors.  For row vectors, multiple distributions
% are plotted.
%
% Returns a handle to the new axis object


% We will get fancy and overlay a second axis on our first one
% This is somewhat complex Matlab code (not beginner stuff), but 
% will produce a nice overlay of the distribution on our noise
% histogram.

% Find the parent of the peer axis so that we may put the new axis
% in the same container.
parent = get(PeerAx, 'Parent');


% Position a second axis on top of our current one.
% Make it transparent so that we can see what's underneath
% turn on the tick marks on the X axis and set up an alternative
% Y axis in another color on the right side.
pdfax = axes('Position', get(PeerAx, 'Position'),  ... % put over
    'Color', 'none', ... % don't fill it in
    'YAxisLocation', 'right', ...  % Y label/axis location
    'XTick', [], ...  % No labels on x axis
    'YColor', 'b', ... % blue
    'Parent', parent);  

X = linspace(xrange(1), xrange(2), 100)';  % N points across xrange
% Preallocate fx assuming one column per pdf
sigma = sqrt(sigma2);  % standard deviation
fx = zeros(length(X), length(mu));

for idx = 1:length(mu)
    fx(:,idx) = normpdf(X, mu(idx), sigma(idx));
end


if length(mu) > 1
    % Replicate Xs for each row so that we have one set of Xs for
    % each one to plot.
    X = repmat(X, [1, length(mu)]);
end

% hold is used to prevent an axis from being reset the next time
% that we plot something
hold(pdfax, 'on');
plot(pdfax, X, fx);



