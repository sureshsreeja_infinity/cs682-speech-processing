ms_per_s = 1000;  % milliseconds per second

% Read in the audio
[x, Fs] = wavread('talking.wav');
% x contains one column for each channel we recorded
% Let's assume we only have a single channel
if size(x, 2) > 1   % size(matrix, d) --> # elements along dim d
    error('Single channel recordings only')
end
samples_N = size(x, 1);



% Create a time axis
t = (0:samples_N-1)/Fs;
figure('Name', 'time domain waveform');
plot(t, x);
xlabel 'Time (s)';
ylabel 'Amplitude (counts)';

% Determine intensity every N ms
framelength_ms = 10;

% convert frame length to s, then compute how many frames we will have
framelength_s = framelength_ms / ms_per_s;
framelength_N = floor(framelength_s * Fs);

frames_N = floor(samples_N / framelength_N);
if rem(samples_N, framelength_N) > 0
    % last frame was not complete, don't use it
    frames_N = frames_N - 1;
end
intensity_dB = zeros(frames_N, 1);  % preallocate matrix for speed
for idx=1:frames_N
    % Extract the frame of speech
    start = (idx-1)*framelength_N+1;
    stop = idx*framelength_N - 1;
    samples = x(start:stop);
    
    % Compute the intensity in dB
    % Note the use of .^ to exponentiate on a per sample
    % basis rather than matrix exponentiation.
    power = sum(samples .^ 2) / length(samples);
    intensity_dB(idx)= 10*log10(power);
end

% plot the intensity
figure('Name', 'Intensity')
t2 = (0:frames_N-1)* framelength_s;  % time axis for intensity
plot(t2, intensity_dB);
xlabel('Time (s)')
% relative as our microphone is not calibrated
ylabel('dB rel.')

figure('Name', 'Intensity Histogram')
hist(intensity_dB, 50)  % hist(data, N) --> histrogram with N bins
intensityax = gca;  % Save current axis for later
xlabel('dB rel.')
ylabel('Counts');

% Noise estimate
% taken from silent section at the beginning
silencestop_s = 1.3;  % start to this time
silencestop_N = floor(silencestop_s / framelength_s);
silence_train = intensity_dB(1:silencestop_N);
figure('Name', 'Silence intensity estimate histogram')
hist(silence_train, 50);
silenceax = gca;  % Save axis of silence distribution
xlabel('dB rel.')
ylabel('Counts');

% Make the noise intensity histogram have the same scale as the
% noise and speech intensity histogram
xrange = get(intensityax, 'XLim');
yrange = get(intensityax, 'YLim');
set(silenceax, 'XLim', xrange);
set(silenceax, 'YLim', yrange);

mu_noise = mean(silence_train);
var_noise = var(silence_train);
display (mu_noise);
fprintf('Silence estimate:  mean %f variance %f\n', mu_noise, var_noise);

% A Normal "bell curve" probability density function (pdf)
% is defined by its mean and variance.
% Let's see how well it fits our noise histogram
noiselikelihoodax = add_normplot(silenceax, xrange, mu_noise, var_noise);

% --------------------------------------------------------------------
% To do at home...
% Fit a normal distribution to the speech


1;



