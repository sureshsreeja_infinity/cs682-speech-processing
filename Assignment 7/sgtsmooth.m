function [rstar, Nrstar, polycoef] = sgtsmooth( r, Nr , useSGT)

    narginchk(2,3);  % Verify input arguments
    if nargin < 3
        useSGT = true;
    end

    Zr = convertToZr( Nr );
    polycoef = polyfit(log10(r), log10(Zr), 1);
    % ------------------------ plots -------------------------------------- 
    figure('Name','Nr vs r');
    scatter(log10(r),log10(Nr));
    xlabel('log10(r)');
    ylabel('log10(Nr)');
    figure('Name','Zr vs r');
    scatter(log10(r),log10(Zr));
    hold on;
    plot(log10(r),polyval(polycoef,log10(r)),'m');
    xlabel('log10(r)');
    ylabel('log10(Zr)');
    hold off;
    % ---------------------------------------------------------------------
    
    % ------------------------ turing estimates ---------------------------
    turingEstimates = r(2:end) .* (Zr(2:end) ./ Zr(1:end-1));

    figure('Name','Turing estimates');
    scatter(log10(turingEstimates), log10(Zr(1:end-1)));
    xlabel('log10(rstar)');
    ylabel('log10(Nrstar)');
    % ---------------------------------------------------------------------
    
    variance = (r(2:end) ./ Zr(1:end-1)) .* ...
        ((Zr(2:end) .* (1 + (Zr(2:end) ./ Zr(1:end-1)))) .^ 0.5);
    
    % ------------------------ LGT estimates ------------------------------
    r(end + 1) = r(end) + 1; %increment r for calculating Nrstar
    Nrstar = 10.^polyval(polycoef,log10(r));
    linearGoodTuringEstimates = r(2:end) .* (Nrstar(2:end) ./ Nrstar(1:end-1));
    % ---------------------------------------------------------------------
    
    if useSGT
        switchingIndex = find(abs(linearGoodTuringEstimates(1:end-1) - turingEstimates) > 1.65 * variance,1);
        rstar = cat(1,turingEstimates(1:switchingIndex-1),linearGoodTuringEstimates(switchingIndex:end));
        figure('Name','Simple Good-Turing method');
        scatter(log10(rstar),log10(Nrstar(1:end-1)));
        xlabel('log10(rstar)');
        ylabel('log10(Nrstar)');
    else
        rstar = linearGoodTuringEstimates;
        figure('Name','Linear Good-Turing estimates');
        scatter(log10(rstar), log10(Nrstar(1:end-1)));
        xlabel('log10(rstar)');
        ylabel('log10(Nrstar)');
    end
end

function [Zr] = convertToZr( Nr )
   Zr = Nr;
   nonZeros = find(Nr);
   for i = 1:length(nonZeros)
       incrementor = 0;
       decrementor = 0;
       if i == 1
           t = 0;
           incrementor = 1;
       else
           t = nonZeros(i-1);
       end
       if i == length(nonZeros)
         q = 2 * i - t;
           decrementor = 1;
       else
           q = nonZeros(i+1);
       end
       z = (2*Nr(nonZeros(i)))/ (q - t);
       Zr (t+incrementor+1:q-decrementor-1) = z;
   end
   
end

